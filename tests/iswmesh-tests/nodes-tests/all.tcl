package require tcltest

tcltest::configure -testdir [file dirname [file normalize [info script]]]
eval tcltest::configure $argv

# Hook to determine if any of the tests failed. Then we can exit with
# proper exit code: 0=all passed, 1=one or more failed
proc tcltest::cleanupTestsHook {} {
    variable numTests

    # Update total failed count.
    if {$numTests(Failed) > 0} {
      source "totalFailedCount.tcl"
      set totalFailedCount [expr {$totalFailedCount + $numTests(Failed)}]
      set fid [open "totalFailedCount.tcl" w]
      puts $fid "set totalFailedCount $totalFailedCount"
      close $fid
    }
}

tcltest::runAllTests
