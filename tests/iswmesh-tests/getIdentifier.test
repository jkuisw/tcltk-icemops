# ::iswmesh::getIdentifier Tests

package require tcltest
namespace import ::tcltest::*

set rootDir [file normalize [file join ".."]]
set packageDir [glob -directory $rootDir -type d "icemops*"]
source [file join $packageDir "iswmesh.tcl"]

# test cases format:
# <max id> <free ids> <expected new id> <expected max id> <expected free ids>
set testCases [list \
  -1 {} 0 0 {} \
  2749 {} 2750 2750 {} \
  845 { 13 78 0 } 13 845 { 78 0 } \
  15 { 21 16 18 19 17 20 } 21 21 { 16 18 19 17 20 } \
]

set i 0
foreach { maxId freeIds expNewId expMaxId expFreeIds } $testCases {
  set testName "getIdentifier_TestCase$i"
  set testDescription [format "%s %d %s %d %s %s %s %d %s %s %s" \
    "Should get a new id of" $expNewId "with expected max id of" $expMaxId \
    "and expected free ids list {" $expFreeIds "} when current max id is" \
    $maxId " and current free ids list is {" $freeIds "}." \
  ]

  test $testName $testDescription -body {
    set newId [::iswmesh::getIdentifier freeIds maxId]

    if {$newId != $expNewId} {
      error "newId($newId) != expNewId($expNewId)"
    }

    if {$maxId != $expMaxId} {
      error "maxId($maxId) != expMaxId($expMaxId)"
    }

    foreach freeId $freeIds expFreeId $expFreeIds {
      if {$freeId != $expFreeId} {
        error "free ids { $freeIds } != expected free ids { $expFreeIds }"
      }
    }
  } -returnCodes {ok}

  incr i
}

cleanupTests
