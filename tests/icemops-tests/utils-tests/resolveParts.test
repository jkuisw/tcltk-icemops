# ::icemops::resolveParts Tests

package require tcltest
namespace import ::tcltest::*

set rootDir [file normalize [file join ".."]]
set packageDir [glob -directory $rootDir -type d "icemops*"]
source [file join $packageDir "icemops-utils.tcl"]

# ------------------------------------------------------------------------------
# Mock ups
set partsList [list \
  "PART_1" \
  "PART_2" \
  "PART_3_PART_4" \
  "PART_INTERIOR_PART_1" \
  "PART_INTERIOR_PART_2" \
  "PART_INTERIOR_PART_3" \
]

# Mock ups for ::icemops namespace
proc ::icemops::doesPartExist { part } {
  global partsList

  foreach var $partsList {
    if {$part == $partsList} { return true }
  }

  return false
}

# Mock ups for ICEMCFD procedures.
proc ic_uns_list_families {} {
  global partsList
  return $partsList
}

# Mock ups for ::tcllogger namespace
proc ::tcllogger::logError { procedure message } {
  error "$procedure\n$message"
}

# ------------------------------------------------------------------------------
# Helper functions


# ------------------------------------------------------------------------------
# Test cases definition and data
# Test cases format:
# <caseDescription> <shouldFail> <returnAllMatches> <partNamesList>
# <expectedResult>
set testCases [list \
  "Check existing full part name." "succeed" false \
  [list "PART_1"] \
  [list "PART_1"] \
  "Check not existing full part name." "fail" false \
  [list "PART_1_NOT_EXISTING"] \
  {} \
  "Check existing regexp part name." "succeed" false \
  [list {PART[0-9_]+}] \
  [list "PART_1"] \
  "Check not existing regexp part name." "fail" false \
  [list {PART_PART[0-9_]+}] \
  {} \
  "Check mixed existing full part name and regexp part name." "succeed" false \
  [list "PART_1" {PART[0-9_]{3}PART[0-9_]+}] \
  [list "PART_1" "PART_3_PART_4"] \
  "Check regexp part name matching for multiple parts." "succeed" true \
  [list {[a-zA-Z0-9_]INTERIOR[a-zA-Z0-9_]}] \
  [list "PART_INTERIOR_PART_1" "PART_INTERIOR_PART_2" "PART_INTERIOR_PART_3"] \
]

# ------------------------------------------------------------------------------
# Tests
set i 0
foreach {
  caseDescription
  shouldFail
  returnAllMatches
  partNamesList
  expectedResult
} $testCases {
  set testName "resolveParts_TestCase$i"
  set testDescription [format "\n%s\n%s %s\n" \
    $caseDescription "parts to match:" $partNamesList
  ]

  if {$shouldFail == "fail"} {
    test $testName $testDescription -body {
      set matchedParts [::icemops::resolveParts \
        $partNamesList $returnAllMatches]
    } -returnCodes {error} -match regexp -result {.*Could not find/resolve.*}
  } else {
    test $testName $testDescription -body {
      set matchedParts [::icemops::resolveParts \
        $partNamesList $returnAllMatches]

      # Check the result.
      foreach matchedPart $matchedParts expectedPart $expectedResult {
        if {$matchedPart != $expectedPart} {
          error [format "%s\n%s %s\n%s %s" \
            "The result does not match with the expected values." \
            "Expected values:" $expectedResult \
            "Result values:" $matchedParts \
          ]
        }
      }
    } -returnCodes {ok}
  }

  incr i
}

cleanupTests
