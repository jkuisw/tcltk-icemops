# package: icemops
# description:
#   This script implements operations for parsing and writing a ANSYS FLUENT
#   mesh (*.msh) file.

# Initialize package.
source [file join [file dirname [info script]] "init-package.tcl"]

# Dependencies
package require Tcl 8.4
package require utils
package require tcllogger

# Define the namespace.
namespace eval ::fluentmesh {
  # Export commands.
  namespace export \
    parse \
    write

  # Define namespace variables.
  # Section types.
  variable sectionTypes
  array set sectionTypes [list \
    {comment} {0} \
    {0} {comment} \
    {header} {1} \
    {1} {header} \
    {dimension} {2} \
    {2} {dimension} \
    {node} {10} \
    {10} {node} \
    {periodic-face} {18} \
    {18} {periodic-face} \
    {cell} {12} \
    {12} {cell} \
    {face} {13} \
    {13} {face} \
    {edge} {11} \
    {11} {edge} \
    {face-tree} {59} \
    {59} {face-tree} \
    {cell-tree} {58} \
    {58} {cell-tree} \
    {face-parents} {61} \
    {61} {face-parents} \
    {zone} {39} \
    {39} {zone} \
  ]

  # Node types.
  variable nodeTypes
  array set nodeTypes [list \
    {virtual} {0} \
    {0} {virtual} \
    {any} {1} \
    {1} {any} \
    {boundary} {2} \
    {2} {boundary} \
  ]

  # Node section header - list indices:
  variable nshliZoneId 0
  variable nshliFirstIndex 1
  variable nshliLastIndex 2
  variable nshliNodesType 3
  variable nshliNodesDimension 4

  # Cell element types.
  variable elementTypes
  array set elementTypes [list \
    {mixed} {0} \
    {0} {mixed} \
    {triangular} {1} \
    {1} {triangular} \
    {tetrahedral} {2} \
    {2} {tetrahedral} \
    {quadrilateral} {3} \
    {3} {quadrilateral} \
    {hexahedral} {4} \
    {4} {hexahedral} \
    {pyramid} {5} \
    {5} {pyramid} \
    {wedge} {6} \
    {6} {wedge} \
    {polyhedral} {7} \
    {7} {polyhedral} \
  ]

  # Cell types.
  variable cellTypes
  array set cellTypes [list \
    {dead-zone} {0} \
    {0} {dead-zone} \
    {fluid} {1} \
    {1} {fluid} \
    {solid} {17} \
    {17} {solid} \
  ]

  # Cell section header - list indices:
  variable cshliZoneId 0
  variable cshliFirstIndex 1
  variable cshliLastIndex 2
  variable cshliCellsType 3
  variable cshliElementType 4

  # Face types.
  variable faceTypes
  array set faceTypes [list \
    {mixed} {0} \
    {0} {mixed} \
    {linear} {2} \
    {2} {linear} \
    {triangular} {3} \
    {3} {triangular} \
    {quadrilateral} {4} \
    {4} {quadrilateral} \
    {polygonal} {5} \
    {5} {polygonal} \
  ]

  # Face section header - list indices:
  variable fshliZoneId 0
  variable fshliFirstIndex 1
  variable fshliLastIndex 2
  variable fshliBoundaryCondType 3
  variable fshliFaceType 4

  # Zone section header - list indices:
  variable zshliZoneId 0
  variable zshliZoneType 1
  variable zshliZoneName 2
  variable zshliDomainId 3
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::fluentmesh::parse { meshfilepath dataArray } {
  upvar $dataArray data

  if {[file exists $meshfilepath] == false} {
    ::tcllogger::logError "::fluentmesh::parse" \
      "The meshfile $meshfilepath does not exist!"
  }

  if {[array exists data] == false} {
    ::tcllogger::logError "::fluentmesh::parse" \
      "The given dataArray parameter is not an array!"
  }

  # load namespace variables
  variable sectionTypes

  if {[catch {open $meshfilepath r} meshfile]} {
    ::tcllogger::logError "::fluentmesh::parse" \
      [format "Failed to read mesh file \"%s\".\n%s" $meshfilepath $meshfile]
  }

  # Status array.
  set status(root) "s_search"
  set status(section) "s_search_header"
  set status(currentSectionType) $sectionTypes(comment)
  set status(nodeCounter) 0
  set status(faceCounter) 0
  set status(zoneCondCounter) 0

  # Data array.
  set data(header) ""
  set data(dimension) 0
  set data(numNodes) 0
  set data(numNodeSections) 0
  set data(numCells) 0
  set data(numCellSections) 0
  set data(numFaces) 0
  set data(numFaceSections) 0
  set data(numZoneSections) 0

  while {[gets $meshfile line] >= 0} {
    switch -exact -- $status(root) {
      "s_search" {
        if {[regexp {\s*\(([0-9]*)\s*} $line match sectionType]} {
          set lineLen [string length $line]
          set matchLen [string length $match]
          set status(root) "s_parse"
          set status(section) "s_search_header"
          set status(currentSectionType) $sectionType

          if {$lineLen > $matchLen} {
            set hasFailed [catch {
              parseSection [string range $line $matchLen end] status data
            } errorMessage]

            if {$hasFailed} {
              catch { close $meshfile }
              ::tcllogger::logError "::fluentmesh::parse" $errorMessage
            }
          }
        }
      }
      "s_parse" {
        set hasFailed [catch { parseSection $line status data } errorMessage]

        if {$hasFailed} {
          catch { close $meshfile }
          ::tcllogger::logError "::fluentmesh::parse" $errorMessage
        }
      }
      default {
        catch { close $meshfile }
        ::tcllogger::logError "::fluentmesh::parse" \
          "Fatal error! Root status $status(root) does not exist!"
      }
    }
  }

  # Close mesh file.
  close $meshfile
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::fluentmesh::parseSection { line statusArray dataArray} {
  upvar $statusArray status
  upvar $dataArray data

  # load namespace variables
  variable sectionTypes

  switch -exact -- $sectionTypes($status(currentSectionType)) {
    comment {
      # ignore comments
      ::tcllogger::log trace "ignore comment: $line"
      set status(root) "s_search"
    }
    header {
      if {[regexp {"(.*)"\)} $line match header]} {
        set data(header) $header
      } else {
        ::tcllogger::log debug [format \
          "Failed to parse header section: \"%s\"" $line]
      }

      set status(root) "s_search"
    }
    dimension {
      if {[regexp {([0-9]+)\)} $line match dim]} {
        set data(dimension) $dim
      } else {
        ::tcllogger::log debug [format \
          "Failed to parse dimension section: \"%s\"" $line]
      }

      set status(root) "s_search"
    }
    node { parseNodeSection $line status data }
    cell { parseCellSection $line status data }
    face { parseFaceSection $line status data }
    zone { parseZoneSection $line status data }
    default {
      ::tcllogger::log warn [format \
        "The section type \"%s\" is not supported, it will be ignored." \
        $sectionTypes($status(currentSectionType))]
    }
  }

}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::fluentmesh::parseNodeSection { line statusArray dataArray} {
  upvar $statusArray status
  upvar $dataArray data

  # load namespace variables
  variable nshliZoneId
  variable nshliFirstIndex
  variable nshliLastIndex
  variable nshliNodesType
  variable nshliNodesDimension

  switch -exact -- $status(section) {
    s_search_header {
      set headerPattern {\(([^)]*)\)([ ]*\)|[ ]*\(|)}
      set isMatch [regexp $headerPattern $line match header beginBody]

      # node section header list (index - description):
      # 0 - zone id
      # 1 - first index
      # 2 - last index
      # 3 - nodes type
      # 4 - nodes dimension
      if {$isMatch} {
        # Convert hex to decimal.
        set hasFailed [catch {
          set header [::utils::lHexToDec $header]
        }]

        if {$hasFailed} {
          ::tcllogger::logError "::fluentmesh::parseNodeSection" [format \
            "Failed to convert node section header values: \"%s\"" $line]
        }

        if {[lindex $header $nshliZoneId] == 0} {
          # zoneId == 0, declaration header parsed.
          set data(numNodes) [lindex $header $nshliLastIndex]

          set status(section) "s_search_header"
          set status(root) "s_search"
        } else {
          # Increment number of node sections.
          incr data(numNodeSections)

          # Build node section index base name.
          set nodeSecBaseName "nodesSection-[expr {$data(numNodeSections) - 1}]"

          # Save the parsed data.
          set data(${nodeSecBaseName}-zoneId) [lindex $header $nshliZoneId]

          set data(${nodeSecBaseName}-firstIndex) \
            [lindex $header $nshliFirstIndex]

          set data(${nodeSecBaseName}-lastIndex) \
            [lindex $header $nshliLastIndex]

          set data(${nodeSecBaseName}-nodesType) \
            [lindex $header $nshliNodesType]

          # Nodes dimension is an optional parameter.
          if {[llength $header] >= 5} {
            set data(${nodeSecBaseName}-nodesDimension) \
              [lindex $header $nshliNodesDimension]
          } else {
            set data(${nodeSecBaseName}-nodesDimension) 0
          }

          # Create nodes list for new node section and initialize node counter.
          set data(${nodeSecBaseName}-nodes) {}
          set status(nodeCounter) $data(${nodeSecBaseName}-firstIndex)

          # Set status for next line.
          if {[regexp {[ ]*\(} $beginBody]} {
            set status(section) "s_parse_body"
          } else {
            set status(section) "s_search_begin_body"
          }
        }
      } else {
        ::tcllogger::logError "::fluentmesh::parseNodeSection" \
          [format "Failed to parse node section: \"%s\"" $line]
      }
    }
    s_search_begin_body {
      if {[regexp {^[ ]*\([ ]*} $line]} {
        # Found start of body, body will start at next line. Therefore set
        # status to parse body.
        set status(section) "s_parse_body"
      }
    }
    s_parse_body {
      # Build node section index base name.
      set nodeSecBaseName "nodesSection-[expr {$data(numNodeSections) - 1}]"

      # Parse body entry.
      set coordinates $line
      if {[llength $coordinates] >= 2} {
        # Save node number and node coordinates.
        lappend data(${nodeSecBaseName}-nodes) [concat \
          $status(nodeCounter) $coordinates]

        # Increment node counter
        incr status(nodeCounter)
      } else {
        # Check if reached end of body or failure.
        if {[regexp {\s*\)\)\s*} $line]} {
          # Reached end of node body and node section, all nodes of section
          # read.
          set status(nodeCounter) 0
          set status(section) "s_search_header"
          set status(root) "s_search"
        } elseif {[regexp {\s*\)\s*} $line]} {
          # Reached end of node body, all nodes of section read. Searching now
          # for section end.
          set status(nodeCounter) 0
          set status(section) "s_search_end_section"
        } else {
          ::tcllogger::logError "::fluentmesh::parseNodeSection" [format \
            "Failed to parse node section body: \"%s\"" $line]
        }
      }
    }
    s_search_end_section {
      if {[regexp {\s*\)\s*} $line]} {
        # Reached end of node section.
        set status(section) "s_search_header"
        set status(root) "s_search"
      }
    }
    default {
      ::tcllogger::log warn [format \
        "Section status \"%s\" not valid in this state or does not exist!" \
        $status(section)
      ]
    }
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::fluentmesh::parseCellSection { line statusArray dataArray} {
  upvar $statusArray status
  upvar $dataArray data

  # load namespace variables
  variable cshliZoneId
  variable cshliFirstIndex
  variable cshliLastIndex
  variable cshliCellsType
  variable cshliElementType

  # Parse cell section header line.
  set headerPattern {\(([^)]*)\)}
  set isMatch [regexp $headerPattern $line match header]

  # node section header list (index - description):
  # 0 - zone id
  # 1 - first index
  # 2 - last index
  # 3 - cells type
  # 4 - element type
  if {$isMatch} {
    # Convert hex to decimal.
    set hasFailed [catch {
      set header [::utils::lHexToDec $header]
    }]

    if {$hasFailed} {
      ::tcllogger::logError "::fluentmesh::parseCellSection" [format \
        "Failed to convert cell section header values: \"%s\"" $line]
    }

    if {[lindex $header $cshliZoneId] == 0} {
      # zoneId == 0, declaration header parsed.
      set data(numCells) [lindex $header $cshliLastIndex]
      set status(root) "s_search"
    } else {
      # Increment number of cell sections.
      incr data(numCellSections)

      # Build cell section index base name.
      set cellSecBaseName "cellsSection-[expr {$data(numCellSections) - 1}]"

      # Save the parsed header data.
      set data(${cellSecBaseName}-zoneId) [lindex $header $cshliZoneId]
      set data(${cellSecBaseName}-firstIdx) [lindex $header $cshliFirstIndex]
      set data(${cellSecBaseName}-lastIdx) [lindex $header $cshliLastIndex]
      set data(${cellSecBaseName}-cellsType) [lindex $header $cshliCellsType]
      set data(${cellSecBaseName}-elementType) \
        [lindex $header $cshliElementType]

      # Create cells list for new cell section.
      set data(${cellSecBaseName}-cells) {}
      set i [lindex $header $cshliFirstIndex]
      set lastIdx [lindex $header $cshliLastIndex]

      while {$i <= $lastIdx} {
        lappend data(${cellSecBaseName}-cells) $i
        incr i
      }

      # Check if reached end of cell section or failure.
      if {[regexp {.*\)\)\s*} $line]} {
        # Reached end of cell section.
        set status(root) "s_search"
      } else {
        ::tcllogger::logError "::fluentmesh::parseCellSection" [format \
          "Failed to parse cell section: \"%s\"" $line]
      }
    }
  } else {
    ::tcllogger::logError "::fluentmesh::parseCellSection" \
      [format "Failed to parse cell section: \"%s\"" $line]
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::fluentmesh::parseFaceSection { line statusArray dataArray} {
  upvar $statusArray status
  upvar $dataArray data

  # load namespace variables
  variable fshliZoneId
  variable fshliFirstIndex
  variable fshliLastIndex
  variable fshliBoundaryCondType
  variable fshliFaceType

  switch -exact -- $status(section) {
    s_search_header {
      set headerPattern {\(([^)]*)\)([ ]*\)|[ ]*\(|)}
      set isMatch [regexp $headerPattern $line match header beginBody]

      # node section header list (index - description):
      # 0 - zone id
      # 1 - first index
      # 2 - last index
      # 3 - boundary condition type
      # 4 - face type
      if {$isMatch} {
        # Convert hex to decimal.
        set hasFailed [catch {
          set header [::utils::lHexToDec $header]
        }]

        if {$hasFailed} {
          ::tcllogger::logError "::fluentmesh::parseFaceSection" [format \
            "Failed to convert face section header values: \"%s\"" $line]
        }

        if {[lindex $header $fshliZoneId] == 0} {
          # zoneId == 0, declaration header parsed.
          set data(numFaces) [lindex $header $fshliLastIndex]

          set status(section) "s_search_header"
          set status(root) "s_search"
        } else {
          # Increment number of face sections.
          incr data(numFaceSections)

          # Build face section index base name.
          set faceSecBaseName "facesSection-[expr {$data(numFaceSections) - 1}]"

          # Save the parsed data.
          set data(${faceSecBaseName}-zoneId) [lindex $header $fshliZoneId]

          set data(${faceSecBaseName}-firstIndex) \
            [lindex $header $fshliFirstIndex]

          set data(${faceSecBaseName}-lastIndex) \
            [lindex $header $fshliLastIndex]

          set data(${faceSecBaseName}-boundaryCondType) \
            [lindex $header $fshliBoundaryCondType]

          set data(${faceSecBaseName}-faceType) [lindex $header $fshliFaceType]

          # Create face list for new face section and initialize face counter.
          set data(${faceSecBaseName}-faces) {}
          set status(faceCounter) $data(${faceSecBaseName}-firstIndex)

          # Set status for next line.
          if {[regexp {[ ]*\(} $beginBody]} {
            set status(section) "s_parse_body"
          } else {
            set status(section) "s_search_begin_body"
          }
        }
      } else {
        ::tcllogger::logError "::fluentmesh::parseFaceSection" \
          [format "Failed to parse face section: \"%s\"" $line]
      }
    }
    s_search_begin_body {
      if {[regexp {^[ ]*\([ ]*} $line]} {
        # Found start of body, body will start at next line. Therefore set
        # status to parse body.
        set status(section) "s_parse_body"
      }
    }
    s_parse_body {
      # Build face section index base name.
      set faceSecBaseName "facesSection-[expr {$data(numFaceSections) - 1}]"

      # Parse body entry.
      set hasFailed [catch {
        set nodeCellNumbers [::utils::lHexToDec $line]
      }]

      if {$hasFailed == 0 && [llength $nodeCellNumbers] >= 4} {
        # Save face, node and cell numbers (amount of node numbers depends on
        # face type).
        lappend data(${faceSecBaseName}-faces) [concat \
          $status(faceCounter) $nodeCellNumbers]

        # Increment node counter
        incr status(faceCounter)
      } else {
        # Check if reached end of body or failure.
        if {[regexp {\s*\)\)\s*} $line]} {
          # Reached end of face body and face section, all faces of section
          # read.
          set status(faceCounter) 0
          set status(section) "s_search_header"
          set status(root) "s_search"
        } elseif {[regexp {\s*\)\s*} $line]} {
          # Reached end of face body, all faces of section read. Searching now
          # for section end.
          set status(faceCounter) 0
          set status(section) "s_search_end_section"
        } else {
          ::tcllogger::logError "::fluentmesh::parseFaceSection" [format \
            "Failed to parse face section body: \"%s\"" $line]
        }
      }
    }
    s_search_end_section {
      if {[regexp {\s*\)\s*} $line]} {
        # Reached end of face section.
        set status(section) "s_search_header"
        set status(root) "s_search"
      }
    }
    default {
      ::tcllogger::log warn [format \
        "Section status \"%s\" not valid in this state or does not exist!" \
        $status(section)
      ]
    }
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::fluentmesh::parseZoneSection { line statusArray dataArray} {
  upvar $statusArray status
  upvar $dataArray data

  # load namespace variables
  variable zshliZoneId
  variable zshliZoneType
  variable zshliZoneName
  variable zshliDomainId

  switch -exact -- $status(section) {
    s_search_header {
      set headerPattern {\(([^)]*)\)([ ]*\([ ]*\)[ ]*\)|[ ]*\(|)}
      set isMatch [regexp $headerPattern $line match header beginBody]

      # node section header list (index - description):
      # 0 - zone id
      # 1 - zone type
      # 2 - zone name
      # 3 - domain id
      if {$isMatch} {
        # Increment number of zone sections.
        incr data(numZoneSections)

        # Build zone section index base name.
        set zoneSecBaseName "zoneSection-[expr {$data(numZoneSections) - 1}]"

        # Save the parsed data.
        set data(${zoneSecBaseName}-zoneId) [lindex $header $zshliZoneId]
        set data(${zoneSecBaseName}-zoneType) [lindex $header $zshliZoneType]
        set data(${zoneSecBaseName}-zoneName) [lindex $header $zshliZoneName]

        # Domain id is maybe not available (but according to spec it should).
        if {[llength $header] >= 4} {
          set data(${zoneSecBaseName}-domainId) [lindex $header $zshliDomainId]
        }

        # Initialize zone condition counter.
        set status(zoneCondCounter) 0

        # Set status for next line.
        if {[regexp {[ ]*\([ ]*\)[ ]*\)} $beginBody]} {
          # Reached end of zone section.
          set status(section) "s_search_header"
          set status(root) "s_search"
        } elseif {[regexp {[ ]*\(} $beginBody]} {
          set status(section) "s_parse_body"
        } else {
          set status(section) "s_search_begin_body"
        }
      } else {
        ::tcllogger::logError "::fluentmesh::parseZoneSection"
          [format "Failed to parse zone section: \"%s\"" $line]
      }
    }
    s_search_begin_body {
      if {[regexp {^[ ]*\([ ]*} $line]} {
        # Found start of body, body will start at next line. Therefore set
        # status to parse body.
        set status(section) "s_parse_body"
      }
    }
    s_parse_body {
      # Build zone section index base name.
      set zoneSecBaseName "zoneSection-[expr {$data(numZoneSections) - 1}]"

      # Parse body entry.
      set bodyPattern {\((\w+) . (\w+)\)([ ]*\)[ ]*\)|[ ]*\)|)}
      set isMatch [regexp $bodyPattern $line match condition value endBody]

      if {$isMatch} {
        # Save zone condition and value.
        lappend data(${zoneSecBaseName}-zoneConditions) [concat \
          $status(zoneCondCounter) $condition $value]

        # Increment zone condition counter
        incr status(zoneCondCounter) 0

        # Check if reached end of body.
        if {[string length $endBody] > 0} {
          if {[regexp {[ ]*\)[ ]*\)} $endBody]} {
            # Reached end of zone body and zone section, all zone conditions of
            # section read.
            set status(zoneCondCounter) 0
            set status(section) "s_search_header"
            set status(root) "s_search"
          } elseif {[regexp {[ ]*\)} $endBody]} {
            # Reached end of zone body, all zone conditions of section read.
            # Searching now for section end.
            set status(zoneCondCounter) 0
            set status(section) "s_search_end_section"
          }
        }
      } else {
        # Check if reached end of body of failure.
        if {[regexp {\s*\)\)\s*} $line]} {
          # Reached end of zone body and zone section, all zone conditions of
          # section read.
          set status(zoneCondCounter) 0
          set status(section) "s_search_header"
          set status(root) "s_search"
        } elseif {[regexp {\s*\)\s*} $line]} {
          # Reached end of zone body, all zone conditions of section read.
          # Searching now for section end.
          set status(zoneCondCounter) 0
          set status(section) "s_search_end_section"
        } else {
          ::tcllogger::logError "::fluentmesh::parseZoneSection" [format \
            "Failed to parse zone section body: \"%s\"" $line]
        }
      }
    }
    s_search_end_section {
      if {[regexp {\s*\)\s*} $line]} {
        # Reached end of zone section.
        set status(section) "s_search_header"
        set status(root) "s_search"
      }
    }
    default {
      ::tcllogger::log warn [format \
        "Section status \"%s\" not valid in this state or does not exist!" \
        $status(section)
      ]
    }
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::fluentmesh::write { meshfilepath dataArray } {
  upvar $dataArray data

  if {[array exists data] == false} {
    ::tcllogger::logError "::fluentmesh::write" \
      "The given dataArray parameter is not an array!"
  }

  # load namespace variables
  variable sectionTypes

  if {[catch {open $meshfilepath w} meshfile]} {
    ::tcllogger::logError "::fluentmesh::write" \
      [format "Failed to create mesh file \"%s\".\n%s" $meshfilepath $meshfile]
  }

  # Write header section.
  if {[info exists data(header)] && $data(header) != ""} {
    puts $meshfile [format "(%d \"%s\")" $sectionTypes(header) $data(header)]
  }

  # Write dimension section.
  if {[info exists data(dimension)] && $data(dimension) != ""} {
    puts $meshfile [format "(%d %d)" $sectionTypes(dimension) $data(dimension)]
  }

  # Write node sections
  writeNodes data $meshfile

  # Write cell sections
  writeCells data $meshfile

  # Write face sections
  writeFaces data $meshfile

  # Write zone sections
  writeZones data $meshfile

  close $meshfile
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::fluentmesh::writeNodes { dataArray meshfile } {
  variable sectionTypes
  upvar $dataArray data

  if {[info exists data(numNodeSections)] && $data(numNodeSections) > 0} {
    # Comment
    puts $meshfile [format "(%d \"Node Sections\")" $sectionTypes(comment)]
    puts $meshfile [format "(%d \"Nodes Declaration Section\")" \
      $sectionTypes(comment)]

    # Nodes declaration section.
    puts $meshfile [format "(%d (0 1 %x 0 %x))" $sectionTypes(node) \
      $data(numNodes) $data(dimension)]

    # Node sections.
    for {set i 0} {$i < $data(numNodeSections)} {incr i} {
      set nodeSecBaseName "nodesSection-$i"

      # Header of node section.
      puts $meshfile [format "(%d (%x %x %x %x %x)" $sectionTypes(node) \
        $data(${nodeSecBaseName}-zoneId) \
        $data(${nodeSecBaseName}-firstIndex) \
        $data(${nodeSecBaseName}-lastIndex) \
        $data(${nodeSecBaseName}-nodesType) \
        $data(${nodeSecBaseName}-nodesDimension) \
      ]

      # Body of node section.
      puts $meshfile "("
      foreach node $data(${nodeSecBaseName}-nodes) {
        # all values except the last one
        foreach value [lrange $node 1 end-1] {
          puts -nonewline $meshfile [format "%.14g " $value]
        }

        # last value
        puts $meshfile [format "%.14g" [lindex $node end]]
      }
      puts $meshfile "))"
    }
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::fluentmesh::writeCells { dataArray meshfile } {
  variable sectionTypes
  upvar $dataArray data

  if {[info exists data(numCellSections)] && $data(numCellSections) > 0} {
    # Comment
    puts $meshfile [format "(%d \"Cell Sections\")" $sectionTypes(comment)]
    puts $meshfile [format "(%d \"Cells Declaration Section\")" \
      $sectionTypes(comment)]

    # Cells declaration section.
    puts $meshfile [format "(%d (0 1 %x 0 0))" $sectionTypes(cell) \
      $data(numCells)]

    # Cell sections.
    for {set i 0} {$i < $data(numCellSections)} {incr i} {
      set cellSecBaseName "cellsSection-$i"

      # Comment
      puts $meshfile [format "(%d \"Cells of zone %s\")" \
        $sectionTypes(comment) \
        [getZoneName data $data(${cellSecBaseName}-zoneId)]]

      # Header of cell section.
      puts $meshfile [format "(%d (%x %x %x %x %x))" $sectionTypes(cell) \
        $data(${cellSecBaseName}-zoneId) \
        $data(${cellSecBaseName}-firstIndex) \
        $data(${cellSecBaseName}-lastIndex) \
        $data(${cellSecBaseName}-cellsType) \
        $data(${cellSecBaseName}-elementType) \
      ]

      # Cell section has no body.
    }
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::fluentmesh::writeFaces { dataArray meshfile } {
  variable sectionTypes
  upvar $dataArray data

  if {[info exists data(numFaceSections)] && $data(numFaceSections) > 0} {
    # Comment
    puts $meshfile [format "(%d \"Face Sections\")" $sectionTypes(comment)]
    puts $meshfile [format "(%d \"Faces Declaration Section\")" \
      $sectionTypes(comment)]

    # Faces declaration section.
    puts $meshfile [format "(%d (0 1 %x 0 0))" $sectionTypes(face) \
      $data(numFaces)]

    # Face sections.
    for {set i 0} {$i < $data(numFaceSections)} {incr i} {
      set faceSecBaseName "facesSection-$i"

      # Comment
      puts $meshfile [format "(%d \"Faces of zone %s\")" \
        $sectionTypes(comment) \
        [getZoneName data $data(${faceSecBaseName}-zoneId)]]

      # Header of face section.
      puts $meshfile [format "(%d (%x %x %x %x %x)" $sectionTypes(face) \
        $data(${faceSecBaseName}-zoneId) \
        $data(${faceSecBaseName}-firstIndex) \
        $data(${faceSecBaseName}-lastIndex) \
        $data(${faceSecBaseName}-boundaryCondType) \
        $data(${faceSecBaseName}-faceType) \
      ]

      # Body of face section.
      puts $meshfile "("
      foreach face $data(${faceSecBaseName}-faces) {
        # all values except the last one
        foreach value [lrange $face 1 end-1] {
          #if {$value < 0} {
            #puts -nonewline $meshfile "0 "
          #} else {
            puts -nonewline $meshfile [format "%x " $value]
          #}
        }

        # last value
        set value [lindex $face end]
        #if {$value < 0} {
          #puts $meshfile "0"
        #} else {
          puts $meshfile [format "%x" $value]
        #}
      }
      puts $meshfile "))"
    }
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::fluentmesh::writeZones { dataArray meshfile } {
  variable sectionTypes
  upvar $dataArray data

  if {[info exists data(numZoneSections)] && $data(numZoneSections) > 0} {
    # Comment
    puts $meshfile [format "(%d \"Zone Sections\")" $sectionTypes(comment)]

    # Zone sections.
    for {set i 0} {$i < $data(numZoneSections)} {incr i} {
      set zoneSecBaseName "zoneSection-$i"

      if {![string match "nodes" $data(${zoneSecBaseName}-zoneType)]} {
        # Header of zone section.
        puts -nonewline $meshfile [format "(%d (%d %s %s)" \
          $sectionTypes(zone) \
          $data(${zoneSecBaseName}-zoneId) \
          $data(${zoneSecBaseName}-zoneType) \
          $data(${zoneSecBaseName}-zoneName) \
        ]

        # Zone section body is not supported in this version.
        puts $meshfile "())"
      }
    }
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::fluentmesh::getZoneName { dataArray zoneId } {
  upvar $dataArray data

  for {set i 0} {$i < $data(numZoneSections)} {incr i} {
    set zoneSecBaseName "zoneSection-$i"

    if {$data(${zoneSecBaseName}-zoneId) == $zoneId} {
      return $data(${zoneSecBaseName}-zoneName)
    }
  }

  return "Unknown"
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
