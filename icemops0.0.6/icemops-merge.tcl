# package: icemops
# description:
#   This script includes functions for merging ANSYS ICEM CFD mesh parts.

# Initialize package.
source [file join [file dirname [info script]] "init-package.tcl"]

# Dependencies
package require Tcl 8.4
package require utils
package require tcllogger

# Define the namespace.
namespace eval ::icemops {
  # Export commands.
  namespace export \
    mergeUnsMeshNodesOfPartPairList

  # Define namespace variables.
  # Save absolute path to the script driectory in a namespace variable.
  variable icemopsMergeScriptDir [file dirname [info script]]
}

# ------------------------------------------------------------------------------
# Merges nodes of an unstructured mesh, located by a part name pair, within a
# given tolerance. The procedure takes a list of part name pairs, which will all
# be merged. A part name pair is a list of two part names and the merge
# tolerance. Node pairs with a distance equal or smaller to the merge tolerance
# will be merged.
# NOTE: From every part name pair all merged elements of the first part will be
#   deleted. If there are no nodes remaining in the first part after the merge,
#   the whole first part will be deleted.
#
# Parameters:
#   partNamePairs - A list of part name pairs that define which nodes will be
#     merged. A part name pair is a list with three items, which must be the
#     names of existing parts and the merge tolerance for these two parts. The
#     part names can be regular expressions which match with existing part
#     names. The list format is:
#     {
#       { <tolerance> <part 1> <part 2> }
#       { <tolerance> <part 3> <part 4> }
#       { <tolerance> <part 5> <part 6> }
#       ...
#     }
#   uniqueOpName - A unique operation name, must be a valid TCL variable name.
#     This string is used for creating temporary files.
#   executor - [default "fluent"] The executor method who will actually do the
#     merging operation. Can be one of: "icemcfd", "fluent", "iswmesh"
#     NOTE: Currently only the executor "fluent" is fully working, the executor
#       "icemcfd" works only for (undefined) small meshes and the executor
#       "iswmesh" is not implemented.
#   shouldCheckMesh - [default false] If after the merge, the mesh should be
#     checked against the diagnostic types defined below.
#   diagnosticTypes - [default ""] Used only when checkMesh is true. A list of
#       diagnostic types used by the check mesh command. If omitted and
#       checkMesh is true, then the following default list will be used:
#       -> duplicate
#       -> uncovered
#       -> missing_internal
#       -> vol_orient
#       -> surf_orient
#       -> hanging
#       -> penetrating
#       -> disconnbars
#       -> triangle_box
#       -> single
#       -> nmanvert
#       -> overlap
#       -> disconnected_vert
#   debug - [default false] If true temporary generated files won't be deleted.
proc ::icemops::mergeUnsMeshNodesOfPartPairList {
  partNamePairs
  uniqueOpName
  {executor "fluent"}
  {shouldCheckMesh false}
  {diagnosticTypes ""}
  {debug false}
} {
  # Check executor.
  switch -exact -- $executor {
    # Only save the resolved list of part names if the executor is fluent.
    "fluent" { set resolvedPartNamePairs {} }
    "iswmesh" {
      ::tcllogger::log info \
        "Currently the executor \"iswmesh\" is not implemented."
      return
    }
    default {
      ::tcllogger::logError "::icemops::mergeUnsMeshNodesOfPartPair" \
        "Unknown executor: \"$executor\""
    }
  }

  # Initialize diagnostic types list.
  if {$shouldCheckMesh == true} {
    if {$diagnosticTypes == ""} {
      # Set default diagnostic types.
      set diagTypes { \
        duplicate \
        uncovered \
        missing_internal \
        vol_orient \
        surf_orient \
        hanging \
        penetrating \
        disconnbars \
        triangle_box \
        single \
        nmanvert \
        overlap \
        disconnected_vert \
      }
    } else {
      # Use user defined diagnostic types.
      set diagTypes $diagnosticTypes
    }

    set partsToCheck {}
  }

  # Iterate through part name pairs.
  foreach partNamePair $partNamePairs {
    # Check if list has three items.
    if {[llength $partNamePair] != 3} {
      ::tcllogger::logError "::icemops::mergeUnsMeshNodesOfPartPairList" \
        [format "%s%s\n%s" \
          "A list entry of the partNamePairs parameter must have three " \
          "entries!" \
          "The list format has to be: { <tolerance> <part 1> <part2> }" \
        ]
    }

    # Check tolerance parameter.
    set tolerance [lindex $partNamePair 0]
    if {[string is double -strict $tolerance] == false} {
      ::tcllogger::logError "::icemops::mergeUnsMeshNodesOfPartPairList" \
        [format "%s%s" \
          "The tolerance must be a floating point number greater than or " \
          "equal to zero!"
        ]
    } elseif {$tolerance < 0} {
      ::tcllogger::logError "::icemops::mergeUnsMeshNodesOfPartPairList" \
        "The tolerance must be greater than or equal to zero!"
    }

    # Check and resolve part names.
    set partNamePair [resolveParts [lrange $partNamePair 1 2]]
    set partName1 [lindex $partNamePair 0]
    set partName2 [lindex $partNamePair 1]

    # Choose the merge executor.
    switch -exact -- $executor {
      "fluent" {
        lappend resolvedPartNamePairs [list $tolerance $partName1 $partName2]
      }
      "icemcfd" {
        # Merge nodes of the parts.
        ::tcllogger::log info \
        [format "%s \"%s\" and \"%s\" within a node distance of %f" \
        "Merging node pairs located by the parts" \
        $partName1 $partName2 $tolerance]

        icemcfdMergeExecutor $partName1 $partName2 $tolerance
      }
    }

    if {$shouldCheckMesh == true} { lappend partsToCheck $partName2 }
  }

  # If executor is fluent, merge nodes now.
  if {$executor == "fluent"} {
    fluentMergeExecutor $resolvedPartNamePairs $uniqueOpName $debug
  }

  # Check the merged mesh.
  if {$shouldCheckMesh == true} {
    set tryToFixProblems true
    if {[checkMesh $diagTypes $tryToFixProblems $partsToCheck] == false} {
      ::tcllogger::logError "::icemops::mergeUnsMeshNodesOfPartPairList" \
        "Mesh check failed!"
    }
  }
}

# ------------------------------------------------------------------------------
# Performs the merge operation in fluent (in meshing mode).
#
# Parameters:
#   partNamePairs - A list of part name pairs that define which nodes will be
#     merged. A part name pair is a list with three items, which must be the
#     names of existing parts and the merge tolerance for these two parts. The
#     list format is:
#     {
#       { <tolerance> <part 1> <part 2> }
#       { <tolerance> <part 3> <part 4> }
#       { <tolerance> <part 5> <part 6> }
#       ...
#     }
#   uniqueOpName - A unique operation name, must be a valid TCL variable name.
#     This string is used for creating temporary files.
#   debug - [default false] If true temporary generated files won't be deleted.
proc ::icemops::fluentMergeExecutor {
  partNamePairs
  uniqueOpName
  {debug false}
} {
  global tcl_platform
  variable icemopsMergeScriptDir
  variable pendingOperations
  set tmpDir [pwd]
  set tmpBoundaryCondFilePath [file join $tmpDir "tmp-${uniqueOpName}.fbc"]

  set tmpUnMeshFileName "tmp-${uniqueOpName}.uns"
  set tmpUnsMeshFilePath [file join $tmpDir $tmpUnMeshFileName]
  set tmpMergedUnsMeshFilePath \
    [file join $tmpDir "merged-$tmpUnMeshFileName"]

  set tmpFluentMeshFileName "tmp-${uniqueOpName}.msh"
  set tmpFluentMeshFilePath [file join $tmpDir $tmpFluentMeshFileName]
  set tmpMergedFluentMeshFilePath \
    [file join $tmpDir "merged-$tmpFluentMeshFileName"]

  # Export mesh to fluent mesh format.
  ic_save_unstruct $tmpUnsMeshFilePath
  ic_boco_save $tmpBoundaryCondFilePath
  ::icemops::exportMeshToFluent $tmpUnsMeshFilePath $tmpFluentMeshFilePath

  # Generate the initialize journal (=scheme) file for fluent, which loads and
  # executes the merge command.
  set schemeScriptFile [file join $tmpDir "init-${uniqueOpName}.scm"]
  set fd [open $schemeScriptFile w]
  set mergeCmdFile [file join $icemopsMergeScriptDir "fluent" "merge.scm"]
  puts $fd "(load \"$mergeCmdFile\")"
  puts $fd "(define input-file \"$tmpFluentMeshFilePath\")"
  puts $fd "(define output-file \"$tmpMergedFluentMeshFilePath\")"
  puts $fd "(merge-parts-of-list input-file output-file (list"

  foreach partNamePair $partNamePairs {
    set tolerance [lindex $partNamePair 0]
    set partName1 [lindex $partNamePair 1]
    set partName2 [lindex $partNamePair 2]
    puts $fd "  (list $tolerance \"$partName1\" \"$partName2\")"
  }

  puts $fd "))"
  catch {close $fd}

  # Execute fluent merging.
  set batFilePath ""
  set fluentExec [file join [getFluentBinDir] "fluent"]
  set cmd [format "\"%s\" 3ddp -meshing -g -i \"%s\"" \
    $fluentExec $schemeScriptFile]

  if {[string match -nocase "*windows*" $tcl_platform(platform)]} {
    # For windows the command has to be wrapped into a *.bat file, which will
    # be executed than. Otherwise there would be some weird problems (e.g.:
    # redirection of stdout not working properly).

    # First create a bat file with the command to execute.
    set batFilePath [::utils::createCmdBatFile $cmd "tmp-${uniqueOpName}-cmd"]

    # Now execute the bat file with the command.
    set cmdChannel [open |[auto_execok $batFilePath]]
  } elseif {[string match -nocase "*unix*" $tcl_platform(platform)]} {
    # On unix platforms all should work as expected.
    set cmdChannel [open "| $cmd" r]
  } else {
    ::tcllogger::logError "::icemops::fluentMergeExecutor" \
      [format "Unsupported platform \"%s\", " \
        $tcl_platform(platform) \
        "only \"unix\" and \"windows\" are supported by this command." \
      ]
  }

  # Register a new pending operation and redirect output of fluent to stdout.
  set pendingOperations($uniqueOpName) "running"
  fconfigure $cmdChannel -blocking No
  fileevent $cmdChannel readable [list ::parallel::redirectToChannel \
    $cmdChannel stdout "::tcllogger::log info" \
    [list ::icemops::cleanUpPendingOps $uniqueOpName $batFilePath] \
  ]

  # Wait for the operation to finish.
  vwait ::icemops::pendingOperations($uniqueOpName)
  array unset pendingOperations $uniqueOpName

  # Convert the merged fluent mesh to the *.uns format.
  convertFluentToUns $tmpMergedFluentMeshFilePath $tmpMergedUnsMeshFilePath \
    $uniqueOpName

  # Close the current mesh and load the merged mesh.
  set isFirst true
  set replace true
  loadUnsMesh3D $uniqueOpName $tmpMergedUnsMeshFilePath $isFirst $replace

  # Delete temporary files.
  if {$debug == false} {
    ::utils::deleteFileIfExists $schemeScriptFile
    ::utils::deleteFileIfExists $tmpBoundaryCondFilePath
    ::utils::deleteFileIfExists "${tmpBoundaryCondFilePath}_old"
    ::utils::deleteFileIfExists $tmpUnsMeshFilePath
    ::utils::deleteFileIfExists $tmpMergedUnsMeshFilePath
    ::utils::deleteFileIfExists $tmpFluentMeshFilePath
    ::utils::deleteFileIfExists $tmpMergedFluentMeshFilePath
  }
}

# ------------------------------------------------------------------------------
# Performs the merge operation in icemcfd. The implementation works only for a
# undefined small number of nodes to merge.
#
# Parameters:
#   partName1 - The name of the first part.
#   partName2 - The name of the second part.
#   tolerance - [default 0] A floating point number defining the merge
#     tolerance.
proc ::icemops::icemcfdMergeExecutor { partName1 partName2 {tolerance 0} } {
  # Create subsets from the interface parts.
  set int1subset [ic_uns_subset_create_family_type $partName1 {__all__}]
  set int2subset [ic_uns_subset_create_family_type $partName2 {__all__}]
  ::tcllogger::log debug \
  "created subset \"$int1subset\" from part \"$partName1\""
  ::tcllogger::log debug \
  "created subset \"$int2subset\" from part \"$partName2\""

  # Get node numbers from the subsets.
  set int1nodes [getNodeNumbersFromSubset $int1subset]
  set int2nodes [getNodeNumbersFromSubset $int2subset]

  # Generate the node-location pairs list for both interface parts.
  set nodeLocPairs1 [getNodeLocationPairsList $int1nodes]
  set nodeLocPairs2 [getNodeLocationPairsList $int2nodes]

  # Un-project all nodes of the interface parts.
  ::tcllogger::log debug "un-project all nodes of the interface parts"
  UnProjectNodes [::utils::lFromNEntry $nodeLocPairs1]
  UnProjectNodes [::utils::lFromNEntry $nodeLocPairs2]

  # Calculate which nodes to merge.
  ::tcllogger::log debug "calculate which nodes to merge"
  set nodeMergePairs [getNodeMergePairs $tolerance $nodeLocPairs1 \
  $nodeLocPairs2]

  #set nodesPerIteration 50000
  #set endPointerPos 0
  #set startPointerPos 0
  set totalNumNodes [llength $nodeMergePairs]

  ::tcllogger::log debug [format "Total number of nodes to merge: %d" \
  $totalNumNodes]
  #while {$startPointerPos < $totalNumNodes} {
  #set endPointerPos [expr {$startPointerPos + $nodesPerIteration - 1}]

  #if {$endPointerPos >= $totalNumNodes} {
  #  set endPointerPos [expr {$totalNumNodes - 1}]
  #}

  #set nodesToMerge [lrange $nodeMergePairs $startPointerPos $endPointerPos]
  #set numNodesToMerge [llength $nodesToMerge]

  #::tcllogger::log debug [format "nodes list pos from %d to %d" \
  $startPointerPos $endPointerPos]
  ::tcllogger::log debug [format "un-project %d nodes" $totalNumNodes]
  UnProjectNodes $nodeMergePairs

  # Merge identified nodes.
  ::tcllogger::log info [format "merging %d nodes" $totalNumNodes]
  ic_uns_merge_node_numbers $nodeMergePairs $tolerance

  #set startPointerPos [expr {$endPointerPos + 1}]
  #}

  #foreach nodeMergePair $nodeMergePairs {
  #  ::tcllogger::log debug [format "merging node pair (%d|%d)" \
  #    [lindex $nodeMergePair 0] [lindex $nodeMergePair 1]]
  #  ic_uns_merge_node_numbers $nodeMergePair $tolerance
  #}

  # Delete elements of first interface part.
  ::tcllogger::log info [format "delete all elements of interface part \"%s\"" \
  $partName1]
  ic_delete_elements subset $int1subset

  # cleanup
  ic_uns_subset_delete $int1subset
  ic_uns_subset_delete $int2subset

  # belongs to "ic_start_bigunsop 1" command above?
  #ic_finish_bigunsop
}

# ------------------------------------------------------------------------------
# Returns a list of node numbers from the nodes which are in the subset
# identified by the given subset name.
#
# Parameters:
#   subsetName - The name of the subset which contains the nodes.
#
# Returns: A list of the node numbers or an empty list if the subset doesn't
#   have any nodes.
proc ::icemops::getNodeNumbersFromSubset { subsetName } {
  set nodeNumbers {}

  # The command "ic_uns_subset_list_elements" returns a list of element data in
  # the following format:
  # {<element type> <element number> <external element number>
  # <list of the associated node numbers> <family (part) name>} {...} ...
  set elemInfoList [ic_uns_subset_list_elements $subsetName]

  ::tcllogger::log trace "elements in the subset \"$subsetName\":"
  if {[lindex [::tcllogger::getParameterValue "logLevel"] 0] >= 4} {
    foreach elem $elemInfoList {
      ::tcllogger::log trace [::utils::indentText "$elem"]
    }
  }

  foreach elemInfo $elemInfoList {
    foreach nodeNum [lindex $elemInfo 3] {
      if {[::utils::lIsDuplicate $nodeNumbers $nodeNum] == false} {
        lappend nodeNumbers $nodeNum
      }
    }
  }

  # Sort the node numbers list in ascending order.
  set nodeNumbers [lsort -integer -increasing $nodeNumbers]
  ::tcllogger::log trace \
    "Node numbers extracted from the subset \"$subsetName\":"
  ::tcllogger::log trace [::utils::indentText "$nodeNumbers"]

  return $nodeNumbers
}

# ------------------------------------------------------------------------------
# Gets the location vector for every node in the given node numbers list and
# returns it as a node - location vector list:
# {{<nodenum1> <locvec1>} {<nodenum2> <locvec2>} ...}
# The loccation vector is a list with coordinates.
#
# Parameters:
#   nodeNumbers - A list of node numbers.
#
# Returns: The node - location vector list.
proc ::icemops::getNodeLocationPairsList { nodeNumbers } {
  set nodeLocPairs {}
  set tmpSubsetName [ic_uns_subset_create]

  # Add nodes identified by node numbers to the subset.
  ic_uns_subset_add_node_numbers $tmpSubsetName $nodeNumbers

  # Build the node number - location pairs list.
  set nodePostitions [ic_uns_get_node_positions $tmpSubsetName]
  foreach nodeNum $nodeNumbers location $nodePostitions {
    lappend nodeLocPairs [list $nodeNum $location]
  }

  # Print the result.
  ::tcllogger::log trace "Node location pairs:"
  if {[lindex [::tcllogger::getParameterValue "logLevel"] 0] >= 4} {
    foreach pair $nodeLocPairs {
      ::tcllogger::log trace [::utils::indentText "$pair"]
    }
  }

  # cleanup
  ic_uns_subset_delete $tmpSubsetName

  return $nodeLocPairs
}

# ------------------------------------------------------------------------------
# Gets a list with all node numbers and their location vectors which are in the
# given subset. The returned node - location vector list has the following
# format:
# {{<nodenum1> <locvec1>} {<nodenum2> <locvec2>} ...}
# The loccation vector is a list with coordinates.
#
# Parameters:
#   subset - The name of the subset which contains the nodes.
#
# Returns: The node - location vector list.
proc ::icemops::getNodeLocationPairsListFromSubset { subset } {
  set nodeLocPairs {}
  set nodesFilePath [file join "." "nodes"]

  # Write nodes (and location vectors) in the subset to a file.
  ic_uns_write_node_list $subset $nodesFilePath

  # Check if the file exists.
  if {[file exists $nodesFilePath] != 1} {
    ::tcllogger::logError "::icemops::getNodeLocationPairsListFromSubset" \
      [format "The node list file \"%s\" does not exist!" $nodesFilePath]
  }

  # Read the nodes (and the location vectors) into the nodeLocPairs list.
  set nodesFile [open $nodesFilePath r]
  set nodePattern {([0-9]+) ([0-9+-.]+) ([0-9+-.]+) ([0-9+-.]+)}

  while {[gets $nodesFile line] >= 0} {
    if {[regexp $nodePattern $line match nodeNumber xcoord ycoord zcoord]} {
      lappend nodeLocPairs [list $nodeNumber $xcoord $ycoord $zcoord]
    }
  }

  # Close file.
  close $nodesFile

  # Print the result.
  ::tcllogger::log trace "Node location pairs:"
  if {[lindex [::tcllogger::getParameterValue "logLevel"] 0] >= 4} {
    foreach pair $nodeLocPairs {
      ::tcllogger::log trace [::utils::indentText "$pair"]
    }
  }

  return $nodeLocPairs
}

# ------------------------------------------------------------------------------
# Determines by the given tolerance which nodes should be merged and returns a
# list of node pairs to merge. For each node in the first node - location vector
# list the distance to each node in the second node - location vector list will
# be calculated. Each node pair, which distance is smaller than or equal to the
# tolerance, will be appended to the list of node pairs to merge.
#
# Parameters:
#   tolerance - A floating point number defining the merge tolerance.
#   nodeLocPairs1 - The first node - location vector list.
#   nodeLocPairs2 - The second node - location vector list.
#
# Returns: A (flat) list of node pairs to merge in the following format:
#   { <pair 1, node 1> <pair 1, node 2> <pair 2, node 1> <pair 2, node 2> ... }
#   e.g.: { 23 115 34 115 }
proc ::icemops::getNodeMergePairs { tolerance nodeLocPairs1 nodeLocPairs2 } {
  set mergePairs {}

  foreach nodePair1 $nodeLocPairs1 {
    foreach nodePair2 $nodeLocPairs2 {
      # Calculate the distance between nodePair1 and nodePair2.
      if {[llength [lindex $nodePair1 1]] != [llength [lindex $nodePair2 1]]} {
        ::tcllogger::logError "::icemops::getNodeMergePairs" \
          "The vectors must have the same length!"
      }
      set distance [::utils::getVectorsDistance [lindex $nodePair1 1] \
        [lindex $nodePair2 1]]

      ::tcllogger::log trace [format "distance for node pair (%d|%d): %f" \
        [lindex $nodePair1 0] [lindex $nodePair2 0] $distance]

      if {$distance <= $tolerance} {
        # Distance between the nodes is smaller than or equal to the tolerance,
        # therefore add nodes to the merge pairs list.
        lappend mergePairs [lindex $nodePair1 0] [lindex $nodePair2 0]
      }
    }
  }

  ::tcllogger::log debug "list of node pairs to merge (node numbers):"
  ::tcllogger::log debug [::utils::indentText "$mergePairs"]

  return $mergePairs
}

# ------------------------------------------------------------------------------
# Un-projects all nodes in the given node numbers list.
#
# Parameters:
#   nodeNumbers - A list of node numbers.
proc ::icemops::UnProjectNodes { nodeNumbers } {
  #set maxInc 1
  set totalNumNodes [llength $nodeNumbers]
  #set nodeCnt 0

  #while {$nodeCnt < $totalNumNodes} {
    #::tcllogger::log debug [format "nodeCnt: %d" $nodeCnt]
    #set deltaNumNodes $maxInc
    #if {[expr {$nodeCnt + $deltaNumNodes}] >= $totalNumNodes} {
    #  set deltaNumNodes [expr {$totalNumNodes - $nodeCnt - 1}]
    #}

    #set endNodeCnt [expr {$nodeCnt + $deltaNumNodes}]
    #set partialNodeNumbers [lrange $nodeNumbers $nodeCnt $endNodeCnt]
    #set pnCnt [llength $partialNodeNumbers]
    set pnCnt $totalNumNodes
    set nodesSubset [ic_uns_subset_create]

    ::tcllogger::log debug [format "add %d node numbers to subset" $pnCnt]
    ic_uns_subset_add_node_numbers $nodesSubset $nodeNumbers 0

    ::tcllogger::log debug [format "un-project %d nodes in subset" $pnCnt]
    ic_uns_set_node_dimension $nodesSubset none

    ::tcllogger::log debug "delete temporary subset"
    ic_uns_subset_delete $nodesSubset

    #set nodeCnt [expr {$nodeCnt + $deltaNumNodes + 1}]
  #}
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
