# package: icemops
# description:
#   This script file implements some ANSYS ICEM CFD utility functions.

# Initialize package.
source [file join [file dirname [info script]] "init-package.tcl"]

# Dependencies
package require Tcl 8.4
package require utils
package require tcllogger

# Define the namespace.
namespace eval ::icemops {
  # Export commands.
  namespace export \
    doesPartExist \
    mergeParts \
    resolveParts \
    checkMesh \
    exportMeshToFluent \
    importFluentMesh \
    convertFluentToUns \
    removePeriodic \
    getFluentBinDir

  # Define namespace variables.
  # Save absolute path to the script driectory in a namespace variable.
  variable icemopsUtilsScriptDir [file dirname [info script]]
  # Array to register pending operations.
  variable pendingOperations
  array set pendingOperations {}
}

# ------------------------------------------------------------------------------
# Renames the families in the given list to the following name format:
#   <component short name>_<new part name list entry>[_<count>]
# e.g.: SEGMENT_WALL_1
#
# Parameters:
#   oldFamilies - List of families to rename.
#   newPartNames - List of new part names.
#   compShortName - A short name for the mesh component.
#   count - [default -1] A count that will be appended to the new name. If count
#     is -1 than it will be ignored.
#
# Returns: A list with the new names.
proc ::icemops::renameFamilies {
  oldFamilies
  newPartNames
  compShortName
  {count -1}
} {
  set newNamesList {}

  # Iterate through families and new part names lists.
  foreach oldFamilyName $oldFamilies newPartName $newPartNames {
    if {$count < 0} {
      # no count
      set newName [format "%s_%s" \
        [string toupper "$compShortName"] \
        [string toupper "$newPartName"]
      ]
    } else {
      # with count
      set newName [format "%s_%s_%d" \
        [string toupper "$compShortName"] \
        [string toupper "$newPartName"] \
        $count
      ]
    }

    # rename family (part)
    ::tcllogger::log debug [::utils::indentText \
      "-> rename family \"$oldFamilyName\" to \"$newName\"" 6]
    #ic_geo_rename_family $oldFamilyName $newName true
    ic_uns_rename_family $oldFamilyName $newName
    ic_delete_empty_parts

    # add name to list
    lappend newNamesList $newName
  }

  # Return the renamed part names as a list.
  return $newNamesList
}

# ------------------------------------------------------------------------------
# Checks if the given part exists.
#
# Parameters:
#   partName - The part to check.
#
# Returns: "true" if the part exists, "false" otherwise.
proc ::icemops::doesPartExist { partName } {
  # Check if the part (family) exists.
  if {[ic_uns_check_family $partName]} {
    # Found the part.
    return true
  }

  # Part does not exist.
  return false
}

# ------------------------------------------------------------------------------
# Merges a list of parts (families) to one part.
#
# Parameters:
#   partsToMerge - A list of parts to merge, must contain at least two parts.
#     The part names can be regular expressions that match with existing part
#     names.
#   newPartName - [default ""] The name of the part the parts are merged
#     into. Can be a name of an existing or a new one (that will be created
#     than). If the name is an empty string, than the first part of the parts
#     list is taken.
proc ::icemops::mergeParts { partsToMerge {newPartName ""} } {
  if {[llength $partsToMerge] <= 1} {
    ::tcllogger::logError \
      "::icemops::mergeParts" "Need at least two parts to merge."
  }

  # Check and resolve the parts to merge.
  set partsToMerge [resolveParts $partsToMerge]

  # If the parameter newPartName is an empty string, take the first part name in
  # the list of parts to merge for the new merged part name.
  if {$newPartName == ""} { set newPartName [lindex $partsToMerge 0] }
  ::tcllogger::log debug \
    "Merging the parts: $partsToMerge into the part \"$newPartName\""

  # Create target family if not already existing.
  ic_uns_new_family $newPartName

  # Move elements of parts to the target part.
  foreach part $partsToMerge {
    set tmpSubset [ic_uns_subset_create_family_type $part {__all__}]
    ic_uns_change_family $tmpSubset $newPartName
    ic_uns_subset_delete $tmpSubset
  }
}

# ------------------------------------------------------------------------------
# Resolves the given list of parts (families). The given list of part names can
# be regular expressions or the full part name.
#
# Parameters:
#   parts - [deault {}] The list of parts to resolve.
#   returnAllMatches - [default false] If true, all matches for an list entry
#     will be returned.
#
# Returns: A list with the resolved parts.
proc ::icemops::resolveParts { {parts {}} {returnAllMatches false} } {
  set resolvedParts {}

  # Loop through the parts to resolve.
  foreach part $parts {
    # Check if the part is a full part name and exists.
    if {[doesPartExist $part] == true} {
      lappend resolvedParts $part
    } else {
      # Part does not exists, therefore loop through the existing parts and try
      # to match the part with existing part names.

      # Get a list of the existing parts.
      set existingPartsList [ic_uns_list_families]
      set resolved false

      foreach existingPart $existingPartsList {
        if {[regexp $part $existingPart]} {
          # Found match.
          lappend resolvedParts $existingPart
          set resolved true

          # Only break after the first match of only one match should be
          # returned.
          if {$returnAllMatches == false} { break }
        }
      }

      # Check if the part could be resolved.
      if {$resolved == false} {
        ::tcllogger::logError "::icemops::resolveParts" \
          [format "Could not find/resolve the part %s." $part]
      }
    }
  }

  return $resolvedParts
}

# ------------------------------------------------------------------------------
# Checks the mesh for problems, according to the given list of diagonstic types
# to check against.
#
# Parameters:
#   diagTypes - A list of diagonstic types to check, possible types are:
#       -> duplicate
#       -> uncovered
#       -> missing_internal
#       -> periodic_problem
#       -> vol_orient
#       -> surf_orient
#       -> hanging
#       -> penetrating
#       -> disconnbars
#       -> multiple
#       -> triangle_box
#       -> single
#       -> nmanvert
#       -> single_2
#       -> single_multiple
#       -> standalone
#       -> delaunay
#       -> overlap
#       -> disconnected_vert
#     For further information see the ANSYS ICEM CFD documentation of the check
#     mesh GUI command and the ic_uns_diagnostic command in the Programming
#     Guide.
#   tryFixProblems - [default true] When true than the ic_uns_diagnostic
#     command tries to fix problems, if there are any.
#   parts - [default ""] A list of existing parts of which the mesh should
#     be checked. The part names can be regular expressions which match with
#     existing part names. If this parameter is omitted, all parts will be
#     checked.
#
# Returns: True if no problems were found, false otherwise.
proc ::icemops::checkMesh {
  diagTypes
  {tryFixProblems true}
  {parts ""}
} {
  # Check diagTypes list parameter.
  if {[llength $diagTypes] <= 0} {
    ::tcllogger::logError "::icemops::checkMesh" \
      "Need at least one diagonstic type to proceed."
  }

  if {$parts != ""} {
    # Resolve part names and check if existing.
    set parts [resolveParts $parts]
  }

  set tryFix 0
  if {$tryFixProblems == true} { set tryFix 1 }

  # Loop through diagnostic types and check the mesh against them.
  foreach diagType $diagTypes {
    if {$parts != ""} {
      set problems [ ic_uns_diagnostic \
        diag_type $diagType \
        fams $parts \
        fix $tryFix \
      ]
    } else {
      set problems [ ic_uns_diagnostic \
        diag_type $diagType \
        fix $tryFix \
      ]
    }

    # If problems were found, abort the check and return false.
    if {[lindex $problems 0] > 0} { return false }
  }

  return true
}

# ------------------------------------------------------------------------------
# Exports the given mesh to a fluent mesh file.
#
# Parameters:
#   meshFilePath - The path to the mesh file to export.
#   outFilePath - The file to write.
#   bocoFilePath - [default ""] Path to the boundary condition file. If this
#     parameter is omitted or the default values is used, then the procedure
#     searches for *.fbc or *.atr (in this order) files, which are named like
#     the mesh file, in the directory where the mesh file is located.
#   boundaryConds - [default {}] The boundary conditions to set. If this
#     parameter is present, the exported mesh will be opened in fluent with
#     meshing mode activated and the given list of boundary conditions are
#     assigned to the parts. After that the mesh saved again in the fluent mesh
#     format. The parameter must be a list in the following format:
#     {
#       { <boundary condition 1> { <part 1> <part 2> <part 3> ... } }
#       { <boundary condition 2> { <part 4> <part 5> <part 6> ... } }
#       ...
#     }
#   debug - [default false] If true temporary generated files won't be deleted.
#
# Returns: 1 if successful, otherwise an error will be thrown.
proc ::icemops::exportMeshToFluent {
  meshFilePath
  outFilePath
  {bocoFilePath ""}
  {boundaryConds {}}
  {debug false}
} {
  global tcl_platform
  variable icemopsUtilsScriptDir
  variable pendingOperations
  # Check if mesh file exists.
  if {![file exists $meshFilePath]} {
    ::tcllogger::logError "::icemops::exportMeshToFluent" \
      "The mesh file \"$meshFilePath\" does not exist!"
  }

  # Check boco file.
  set resolvedBocoFilePath $bocoFilePath
  if {$bocoFilePath == ""} {
    # Try to find boco file by mesh file name.
    set prjName [file tail [file rootname $meshFilePath]]
    set prjDir [file dirname $meshFilePath]

    if {[file exists [file join $prjDir "${prjName}.fbc"]]} {
      set resolvedBocoFilePath [file join $prjDir "${prjName}.fbc"]
    } elseif {[file exists [file join $prjDir "${prjName}.atr"]]} {
      set resolvedBocoFilePath [file join $prjDir "${prjName}.atr"]
    } else {
      ::tcllogger::logError "::icemops::exportMeshToFluent" \
        "Could not find boundary condition file!"
    }
  }

  ::tcllogger::log info "meshFilePath = $meshFilePath"
  ::tcllogger::log info "outFilePath = $outFilePath"
  ::tcllogger::log info "bocoFilePath = $bocoFilePath"
  ::tcllogger::log info "boundaryConds = $boundaryConds"

  # Export mesh.
  ic_export_mesh \
    format fluent6 \
    domfile $meshFilePath \
    bcfile $resolvedBocoFilePath \
    file $outFilePath \
    parfile 0

  # Set boundary conditions if there are any.
  if {[llength $boundaryConds] >= 1} {
    # Generate a unique operation name.
    set uniqueOpName [file rootname [file tail $meshFilePath]]
    set uniqueOpName "set-boundary-conds-$uniqueOpName"

    # Generate the initialize journal (=scheme) file for fluent, which loads and
    # executes the set-boundary-conds command.
    set schemeScriptFile [file join [pwd] "init-${uniqueOpName}.scm"]
    set fd [open $schemeScriptFile w]
    set boundCondFile [file join $icemopsUtilsScriptDir "fluent" \
      "set-boundary-conds.scm"]
    puts $fd "(load \"$boundCondFile\")"
    puts $fd "(define input-file \"$outFilePath\")"
    puts $fd "(define output-file \"$outFilePath\")"
    puts $fd "(set-boundary-conds input-file output-file (list"

    foreach boundaryCond $boundaryConds {
      set boundCondName [lindex $boundaryCond 0]
      set parts [lindex $boundaryCond 1]

      puts $fd "  (list \"$boundCondName\" (list"
      foreach part $parts { puts $fd "    \"$part\"" }
      puts $fd "  ))"
    }

    puts $fd "))"
    catch {close $fd}

    # Execute setting of boundary conditions in fluent.
    set batFilePath ""
    set fluentExec [file join [getFluentBinDir] "fluent"]
    set cmd [format "\"%s\" 3ddp -meshing -g -i \"%s\"" \
      $fluentExec $schemeScriptFile]

    if {[string match -nocase "*windows*" $tcl_platform(platform)]} {
      # For windows the command has to be wrapped into a *.bat file, which will
      # be executed than. Otherwise there would be some weird problems (e.g.:
      # redirection of stdout not working properly).

      # First create a bat file with the command to execute.
      set batFilePath [::utils::createCmdBatFile $cmd "tmp-${uniqueOpName}-cmd"]

      # Now execute the bat file with the command.
      set cmdChannel [open |[auto_execok $batFilePath]]
    } elseif {[string match -nocase "*unix*" $tcl_platform(platform)]} {
      # On unix platforms all should work as expected.
      set cmdChannel [open "| $cmd" r]
    } else {
      ::tcllogger::logError "::icemops::exportMeshToFluent" \
        [format "Unsupported platform \"%s\", " \
          $tcl_platform(platform) \
          "only \"unix\" and \"windows\" are supported by this command." \
        ]
    }

    # Register a new pending operation and redirect output of fluent to stdout.
    set pendingOperations($uniqueOpName) "running"
    fconfigure $cmdChannel -blocking No
    fileevent $cmdChannel readable [list ::parallel::redirectToChannel \
      $cmdChannel stdout "::tcllogger::log info" \
      [list ::icemops::cleanUpPendingOps $uniqueOpName $batFilePath] \
    ]

    # Wait for the operation to finish.
    vwait ::icemops::pendingOperations($uniqueOpName)
    array unset pendingOperations $uniqueOpName

    # Delete temporary files.
    if {$debug == false} {
      ::utils::deleteFileIfExists $schemeScriptFile
    }
  }
}

# ------------------------------------------------------------------------------
# Imports a fluent *.msh file into icemcfd.
# Note: This works only of no mesh is currently loaded, because of icemcfd (I
# don't know why, stupid icem). Therefore use the procedure
# ::icemops::convertFluentToUns if you want to load than one mesh.
#
# Parameters:
#   meshFilePath - The path to the fluent mesh file to import.
#
# Returns: 1 if successful, otherwise an error will be thrown.
proc ::icemops::importFluentMesh { meshFilePath } {
  # Check if mesh file exists.
  if {![file exists $meshFilePath]} {
    ::tcllogger::logError "::icemops::importFluentMesh" \
      "The mesh file \"$meshFilePath\" does not exist!"
  }

  # Import fluent mesh.
  ic_read_external readfluent $meshFilePath 0
}

# ------------------------------------------------------------------------------
# Converts a fluent .*msh mesh file into a icemcfd *.uns mesh file. This is
# done by starting an icemcfd instance in batch mode. This instance will than
# execute the "convertFluentToUns.tcl" script, which creates the converted
# *.uns mesh file and exit.
#
# Parameters:
#   fluentMeshFilePath -  The fluent *.msh mesh file which should be converted
#     to the *.uns mesh file.
#   unsMeshFilePath -  The *.uns mesh file which should be created.
#   uniqueOpName - A unique operation name, must be a valid TCL variable name.
#     This string is used for creating temporary files.
proc ::icemops::convertFluentToUns {
  fluentMeshFilePath
  unsMeshFilePath
  uniqueOpName
} {
  global env
  global tcl_platform
  variable icemopsUtilsScriptDir

  ::tcllogger::log info "fluentMeshFilePath = $fluentMeshFilePath"
  ::tcllogger::log info "unsMeshFilePath = $unsMeshFilePath"
  ::tcllogger::log info "uniqueOpName = $uniqueOpName"

  # Check if mesh file exists.
  if {![file exists $fluentMeshFilePath]} {
    ::tcllogger::logError "::icemops::convertFluentToUns" \
      "The mesh file \"$fluentMeshFilePath\" does not exist!"
  }

  # Check extension of target file.
  set targetFile $unsMeshFilePath
  set targetFileExt [file extension $unsMeshFilePath]
  if {$targetFileExt == "" || $targetFileExt != ".uns"} {
    set fileDir [file dirname $targetFile]
    set fileName [file tail [file rootname $targetFile]]
    set targetFile [file join $fileDir "${fileName}.uns"]
  }

  # Convert the mesh by starting an icemcfd instance in batch mode which
  # executes the script "convertFluentToUns.tcl".
  ::tcllogger::log info "Converting fluent mesh..."

  # Build the command.
  set cmd [format "\"%s\" -batch -script \"%s\" \"%s\" \"%s\" \"%s\"" \
    [file join $env(ICEM_ACN) bin icemcfd] \
    [file join $icemopsUtilsScriptDir "icem-batchmode" \
      "convertFluentToUns.tcl"] \
    $fluentMeshFilePath \
    $targetFile \
    [lindex [::tcllogger::getParameterValue "logLevel"] 1] \
  ]

  # Execute the command.
  if {[string match -nocase "*windows*" $tcl_platform(platform)]} {
    # For windows the command has to be wrapped into a *.bat file, which will
    # be executed than. Otherwise there would be some weird problems (e.g.:
    # redirection of stdout not working properly).

    # First create a bat file with the command to execute.
    set batFilePath [::utils::createCmdBatFile $cmd "tmp-${uniqueOpName}-cmd"]

    # Now execute the bat file with the command.
    set cmdChannel [open |[auto_execok $batFilePath]]
    #auto_execok $batFilePath

    # Delete *.bat file, because it's not needed anymore.
    #::utils::deleteFileIfExists $batFilePath
  } elseif {[string match -nocase "*unix*" $tcl_platform(platform)]} {
    # On unix platforms all should work as expected.
    set cmdChannel [open "| $cmd" r]
    #exec $cmd
  } else {
    ::tcllogger::logError "::icemops::convertFluentToUns" \
      [format "Unsupported platform \"%s\", " \
        $tcl_platform(platform) \
        "only \"unix\" and \"windows\" are supported by this command." \
      ]
  }

  # Register a new pending operation and redirect output to stdout.
  set pendingOperations($uniqueOpName) "running"
  fconfigure $cmdChannel -blocking No
  fileevent $cmdChannel readable [list ::parallel::redirectToChannel \
    $cmdChannel stdout "::tcllogger::log info" \
    [list ::icemops::cleanUpPendingOps $uniqueOpName $batFilePath] \
  ]

  # Wait for the operation to finish.
  vwait ::icemops::pendingOperations($uniqueOpName)
  array unset pendingOperations $uniqueOpName
}

# ------------------------------------------------------------------------------
# Makes the nodes in the given list of parts (part names) non periodic. Only the
# part which includes one of the two periodically linked nodes must be given.
# e.g.: The nodes in PART1 are periodically linked with the nodes in PART2.
# Therefore pass either PART1 or PART2 to this procedure.
#
# Parameters:
#   parts - [default {}] A list of part names.
proc ::icemops::removePeriodic { {parts {}}} {
  set subset [ic_uns_subset_from_part $parts true]
  ic_uns_remove_periodic $subset
}

# ------------------------------------------------------------------------------
# Returns the directory where the fluent executable is located. This works only
# if the procedure is executed in an ICEMCFD instance. Otherwise the ICEMCFD
# path could not be resolved.
#
# Returns: The directory where the fluent executable is located.
proc ::icemops::getFluentBinDir {} {
  global env
  global tcl_platform
  set icemPath $env(ICEM_ACN)

  if {[string match -nocase "*windows*" $tcl_platform(platform)]} {
    return [file normalize [lindex [glob \
      [file join $icemPath ".." ".." "fluent" "ntbin" "win*"] \
    ] 0]]
  } elseif {[string match -nocase "*unix*" $tcl_platform(platform)]} {
    return [file normalize [file join $icemPath ".." ".." "fluent" "bin"]]
  } else {
    ::tcllogger::logError "::icemops::getFluentBinDir" \
      [format "Unsupported platform \"%s\", " \
        $tcl_platform(platform) \
        "only \"unix\" and \"windows\" are supported by this command." \
      ]
  }
}

# ------------------------------------------------------------------------------
# FOR INTERNAL USE ONLY! Called when a pending operation has finished executing.
# Deletes the *.bat file if any exists and sets the pending operation to
# finished, so that the executor who started the operation can continue.
#
# Parameters:
#   opName - The name of the pending operation.
#   batFilePath - Path to the probably created *.bat file.
proc ::icemops::cleanUpPendingOps { opName batFilePath } {
  variable pendingOperations

  # Set pending operation to finished.
  set pendingOperations($opName) "finished"

  # Delete *.bat file, because it's not needed anymore.
  ::utils::deleteFileIfExists $batFilePath
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
