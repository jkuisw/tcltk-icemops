# package: icemops
# description:
#   This script file implements a custom mesh data model and operations.

# Initialize package.
source [file join [file dirname [info script]] "init-package.tcl"]

# Dependencies
package require Tcl 8.4
package require utils
package require tcllogger

# Define the namespace.
namespace eval ::iswmesh::faces {
  # ----------------
  # Export commands.
  namespace export \
    initialize \
    createFaces \
    addFaces \
    updateFaces \
    updateMergedNodeOfFace \
    updateFaceReferences \
    getFacesByZoneRefIds \
    getNodeRefsOfFacesInZones \
    convertFacesToPreparsed \
    printFaces

  # ---------------------------
  # Define namespace variables.
  variable freeIds {}
  variable maxId -1
  variable maxFaceNumber 0

  # -------------------------------------------------
  # Data inidces for a face entry of the faces array.
  # Face data list index where the face number is stored.
  variable faceNumberIdx 0
  # Face data list index where the zone reference id is stored.
  variable zoneRefIdIdx 1
  # Face data list index where the boundary condition type is stored.
  variable boundaryCondTypeIdx 2
  # Face data list index where the face type is stored.
  variable faceTypeIdx 3
  # Face data list index where the status information list of the face is
  # stored.
  variable statusInfoListIdx 4
  # Face data list index where the reference id to the first cell is stored.
  variable cell0RefIdIdx 5
  # Face data list index where the reference id to the second cell is stored.
  variable cell1RefIdIdx 6
  # Face data list index where the number of node reference ids for the face is
  # stored.
  variable numNodeRefIdsIdx 7
  # Face data list index where the first node reference id is stored is stored.
  variable firstNodeRefIdIdx 8

  # ----------------
  # The faces array.
  variable faces
  array set faces {}

  # --------------------------------------------------------------
  # List indices definitions for the face status information list.
  # Face status info list index where the number of merged nodes is stored. This
  # is not a total number. It defines the number of face nodes which where
  # merged. If all face nodes were merged, then the face has to be deleted,
  # because otherwise there would be two faces for the same nodes.
  variable numNodesMergedIdx 0
  # Face status info list index where the merge count for the first face node is
  # stored. It defines how often the first node was merged (normally only once).
  # The counts for the other nodes are subsequent to this index, in the same
  # order and amount as the node reference ids in the face data list.
  variable firstNodeMergeCntIdx 1

  # ----------------------------------------------------------------
  # Array which maps the face number (key) with the face id (value).
  variable faceNumIdMap
  array set faceNumIdMap {}
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
# { faceNumber zoneId boundaryCondType faceType n0 ... nN c0 c1 }
# Returns:
proc ::iswmesh::faces::initialize { {newFaces {}} } {
  variable freeIds
  variable maxId
  variable maxFaceNumber
  variable faces
  variable faceNumIdMap

  array unset faces *
  array unset faceNumIdMap *
  set freeIds {}
  set maxId -1
  set maxFaceNumber 0

  addFaces $newFaces
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
# { zoneId boundaryCondType faceType n0 ... nN c0 c1 }
# Returns:
proc ::iswmesh::faces::createFaces { newFaces } {
  variable maxFaceNumber

  foreach newFace $newFaces {
    addFaces [list [concat [incr maxFaceNumber] $newFace]]
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
# { faceNumber zoneId boundaryCondType faceType n0 ... nN c0 c1 }
# Returns:
proc ::iswmesh::faces::addFaces { newFaces } {
  variable freeIds
  variable maxId
  variable maxFaceNumber
  variable faces
  variable faceNumIdMap
  variable ::fluentmesh::faceTypes

  foreach newFace $newFaces {
    # Get a face id.
    set newId [::iswmesh::getIdentifier freeIds maxId]

    # Determine the number of nodes for this face (depends on the face type).
    set faceType [lindex $newFace 3]
    set numNodes 0
    set firstNodeIdx 4

    switch -exact -- $::fluentmesh::faceTypes($faceType) {
      mixed -
      polygonal {
        set numNodes [lindex $newFace 4]
        set firstNodeIdx 5
      }
      linear { set numNodes 2 }
      triangular { set numNodes 3 }
      quadrilateral { set numNodes 4 }
    }

    # Get other face values.
    set faceNumber [lindex $newFace 0]
    set zoneId [lindex $newFace 1]
    set boundaryCondType [lindex $newFace 2]
    set statusInfoList [::utils::lInit [expr {$numNodes + 1}]]
    set lastNodeIdx [expr {$firstNodeIdx + $numNodes - 1}]
    set nodeNumbers [lrange $newFace $firstNodeIdx $lastNodeIdx]
    set cellNumbers [lrange $newFace end-1 end]

    # Check if face already exists.
    if {[info exists faceNumIdMap($faceNumber)]} {
      ::tcllogger::logError "::iswmesh::faces::addFaces" \
        [format "Face number %d already exists!" $faceNumber]
    }

    if {$faceNumber > $maxFaceNumber} { set maxFaceNumber $faceNumber}

    # Resolve zoneId and set a reference.
    set zoneRefId [lindex [::iswmesh::zones::setRefsByZoneId [list $zoneId]] 0]

    # Resolve node numbers and set a reference.
    set faceIdNodeNumberPairs {}
    set nodePosition 0
    foreach nodeNum $nodeNumbers {
      lappend faceIdNodeNumberPairs [list $newId $nodeNum $nodePosition]
      incr nodePosition
    }
    set nodeRefIds [::iswmesh::nodes::setRefsByNodeNumber $faceIdNodeNumberPairs]

    # Resove cell numbers and set a reference.
    set cellRefIds {}
    set cell0 [lindex $cellNumbers 0]
    set cell1 [lindex $cellNumbers 1]

    if {$cell0 > 0} {
      lappend cellRefIds [::iswmesh::cells::setRefsByCellNumber $cell0]
    } else {
      lappend cellRefIds -1
    }

    if {$cell1 > 0} {
      lappend cellRefIds [::iswmesh::cells::setRefsByCellNumber $cell1]
    } else {
      lappend cellRefIds -1
    }

    # Add the new face to the faces array.
    set faces($newId) [concat \
      $faceNumber \
      $zoneRefId \
      $boundaryCondType \
      $faceType \
      [list $statusInfoList] \
      $cellRefIds \
      $numNodes \
      $nodeRefIds \
    ]

    # Add the face to the face number - id map array.
    set faceNumIdMap($faceNumber) $newId
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
# { id zoneId boundaryCondType }
# Returns:
proc ::iswmesh::faces::updateFaces { updatedFaces } {
  variable zoneRefIdIdx
  variable boundaryCondTypeIdx
  variable faces

  foreach updatedFace $updatedFaces {
    set id [lindex $updatedFace 0]

    if {![info exists faces($id)]} {
      ::tcllogger::logError "::iswmesh::faces::updateFaces" \
        [format "Face with id %d does not exist!" $id]
    }

    set newZoneRefId [lindex $updatedFace 1]
    set currentZoneRefId [lindex $faces($id) $zoneRefIdIdx]

    # Check if zone has changed.
    if {newZoneRefId != currentZoneRefId} {
      # Detected new zone, therefore remove reference to old one and set a
      # reference to the new one.
      ::iswmesh::zones::deleteRefsByZoneRefId [list $currentZoneRefId]
      ::iswmesh::zones::setRefsByZoneRefId [list $newZoneRefId]
    }

    # Update the faces data.
    set faces($id) [lreplace $faces($id) $zoneRefIdIdx $boundaryCondTypeIdx \
      [lrange $updatedFace 1 2]
    ]
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::faces::updateMergedNodeOfFace {
  faceId
  nodePos
  currNodeId
  newNodeId
} {
  variable faceNumberIdx
  variable zoneRefIdIdx
  variable statusInfoListIdx
  variable cell0RefIdIdx
  variable cell1RefIdIdx
  variable numNodeRefIdsIdx
  variable firstNodeRefIdIdx
  variable faces
  variable faceNumIdMap
  variable numNodesMergedIdx
  variable firstNodeMergeCntIdx

  set face $faces($faceId)
  set statusInfo [lindex $face $statusInfoListIdx]

  # Calculate node merge count position.
  set nodeMergeCntIdx [expr {$firstNodeMergeCntIdx + $nodePos}]

  # Get node merge count.
  set nodeMergeCount [lindex $statusInfo $nodeMergeCntIdx]

  # Check if node was merged before.
  if {$nodeMergeCount == 0} {
    # First time this node is merged.
    set numNodesMerged [lindex $statusInfo $numNodesMergedIdx]
    lset statusInfo $numNodesMergedIdx [incr numNodesMerged]
  }

  # Increase merge count of node.
  lset statusInfo $nodeMergeCntIdx [incr nodeMergeCount]

  # Update the node reference.
  ::iswmesh::nodes::deleteRefsByNodeId [list [list $faceId $currNodeId]]
  lset face [expr {$firstNodeRefIdIdx + $nodePos}] $newNodeId
  ::iswmesh::nodes::setRefsByNodeId [list [list $faceId $newNodeId $nodePos]]

  # Update the face data.
  lset face $statusInfoListIdx $statusInfo
  set faces($faceId) $face

  # Check if face should be deleted.
  set numNodes [lindex $face $numNodeRefIdsIdx]
  if {[lindex $statusInfo $numNodesMergedIdx] == $numNodes} {
    # All nodes of face are merged, therefore face has to be deleted.
    set faceCell0RefId [lindex $face $cell0RefIdIdx]
    set faceCell1RefId [lindex $face $cell1RefIdIdx]
    set cellId -1

    # A face that was merged has only one cell. It is either the first or
    # the second.
    if {$faceCell0RefId >= 0} {
      set cellId $faceCell0RefId
    } else {
      set cellId $faceCell1RefId
    }

    # Extract the node reference ids of the face.
    set lastNodeRefIdIdx [expr {$firstNodeRefIdIdx + $numNodes - 1}]
    set faceNodeRefIds [lrange $face $firstNodeRefIdIdx $lastNodeRefIdIdx]

    # Delete the node references of the face.
    foreach nodeId $faceNodeRefIds {
      ::iswmesh::nodes::deleteRefsByNodeId [list [list $faceId $nodeId]]
    }

    # Delete the zone reference of the face.
    ::iswmesh::zones::deleteRefsByZoneRefId [list [lindex $face $zoneRefIdIdx]]

    # All references should now be deleted, therefore delete the face.
    array unset faceNumIdMap [lindex $face $faceNumberIdx]
    array unset faces $faceId
    lappend freeIds $faceId

    # Find a new face for the cell which was referenced by the deleted face.
    set foundMatch 0

    # The face for the cell must have been registert to the new node. Therefore
    # check all faces which have a reference to the new node.
    set referencesToCheck [lindex \
      [::iswmesh::nodes::getNodeById $newNodeId] \
      $::iswmesh::nodes::referencesIdx
    ]

    # Iterate through all faces, there has to be a matching face for the cell.
    foreach reference $referencesToCheck {
      set newFaceId [lindex $reference 0]
      set newFace $faces($newFaceId)
      set newFaceNumNodes [lindex $newFace $numNodeRefIdsIdx]

      # Check if the current face has the same number of nodes as the face
      # which was deleted.
      if {$newFaceNumNodes > 0 && $newFaceNumNodes == $numNodes} {
        set newFaceNodeRefIds [lrange $newFace \
          $firstNodeRefIdIdx $lastNodeRefIdIdx]

        # Check if the node ids of the current face matching with that of the
        # deleted face.
        set foundMatch 1
        if {[llength $faceNodeRefIds] == 0} { set foundMatch 0 }
        foreach nodeId $faceNodeRefIds {
          if {[lsearch -exact -integer $newFaceNodeRefIds $nodeId] < 0} {
            # Did not found node id. Therefore the current face is not the
            # matching one.
            set foundMatch 0
            break
          }
        }

        # Check if found a match.
        if {$foundMatch} {
          # A face that was merged has only one cell. It is either the first or
          # the second.
          if {[lindex $faces($newFaceId) $cell0RefIdIdx] < 0} {
            set faces($newFaceId) [lreplace $faces($newFaceId) \
              $cell0RefIdIdx $cell0RefIdIdx $cellId]
          } else {
            set faces($newFaceId) [lreplace $faces($newFaceId) \
              $cell1RefIdIdx $cell1RefIdIdx $cellId]
          }

          break
        }
      }
    }

    # Check again if found a match.
    if {!$foundMatch} {
      ::tcllogger::logError "::iswmesh::faces::updateMergedNodeOfFace" [format \
        "%s %d!\n%s %s\n%s %d\n%s %d\n%s %s" \
        "Couldn't find a matching face for the cell with the id" $cellId \
        "Checked the following references ({<faceId> <nodePos>} ...):" \
        $referencesToCheck \
        "The old node reference id was:" $currNodeId \
        "The new node reference id is:" $newNodeId \
        "Data of the deleted face:" $face \
      ]
    }
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::faces::updateFaceReferences {
  faceIdsWithMergedNodes
  faceIdsMergedTo
} {
  variable faceNumberIdx
  variable zoneRefIdIdx
  variable cell0RefIdIdx
  variable cell1RefIdIdx
  variable numNodeRefIdsIdx
  variable firstNodeRefIdIdx
  variable faces
  variable faceNumIdMap
  set cellsSearchForFaces {}

  # Iterate through face ids with merged nodes to check node references.
  set ts1_1 [clock clicks -milliseconds]
  foreach faceId $faceIdsWithMergedNodes {
    set face $faces($faceId)
    set numNodes [lindex $face $numNodeRefIdsIdx]
    set lastNodeRefIdIdx [expr {$firstNodeRefIdIdx + $numNodes - 1}]
    set currentFaceNodes [lrange $face $firstNodeRefIdIdx $lastNodeRefIdIdx]

    # Check the face node references. If there are changed nodes, then the
    # reference of the current node will be deleted by the command.
    set checkResult [::iswmesh::nodes::checkRefsById $faceId $currentFaceNodes]
    set numNodesChanged [lindex $checkResult 0]

    if {$numNodesChanged >= $numNodes} {
      # All nodes of face are merged, therefore face has to be deleted.
      set faceCell0RefId [lindex $face $cell0RefIdIdx]
      set faceCell1RefId [lindex $face $cell1RefIdIdx]
      set searchEntry {}

      # A face that was merged has only one cell. It is either the first or
      # the second.
      if {$faceCell0RefId >= 0} {
        lappend searchEntry $faceCell0RefId
      } else {
        lappend searchEntry $faceCell1RefId
      }

      # Add the node references to which the nodes were merged, to the
      # search entry. With this node references the face which the cell
      # belongs to can be found.
      set searchEntry [concat $searchEntry [lrange $checkResult 1 end]]

      # Add the search entry.
      lappend cellsSearchForFaces $searchEntry

      # Delete zone reference.
      ::iswmesh::zones::deleteRefsByZoneRefId [list \
        [lindex $face $zoneRefIdIdx]]

      # All references should now be deleted, therefore delete the face.
      array unset faceNumIdMap [lindex $face $faceNumberIdx]
      array unset faces $faceId
      lappend freeIds $faceId
    } elseif {$numNodesChanged > 0} {
      # Only some nodes have been changed, but not all of the face.
      # Therefore the face will not be deleted, only update the node
      # references.
      for {set i 0} {$i < $numNodes} {incr i} {
        set newNodeRefId [lindex $checkResult [expr {$i+1}]]
        set currNodeRefId [lindex $face [expr {$firstNodeRefIdIdx+$i}]]

        if {$newNodeRefId >= 0} {
          ::tcllogger::log trace \
            [format "Change face node ref id (from -> to): %d -> %d" \
            $currNodeRefId $newNodeRefId]

          # A changed node, therefore update the reference.
          ::iswmesh::nodes::setRefsByNodeId \
            [list [list $faceId $newNodeRefId $i]]
          set face [lreplace $face [expr {$firstNodeRefIdIdx+$i}] \
            [expr {$firstNodeRefIdIdx+$i}] $newNodeRefId]
        }
      }

      set faces($faceId) $face
    }
  }
  ::tcllogger::log debug [format \
    "-> loop over faceIdsWithMergedNodes needed %d milliseconds" \
    [expr {[clock clicks -milliseconds] - $ts1_1}]
  ]

  # Find new faces for the cells which were referenced by the deleted faces.
  set ts2_1 [clock clicks -milliseconds]
  foreach searchEntry $cellsSearchForFaces {
    set cellId [lindex $searchEntry 0]
    set nodeIds [lrange $searchEntry 1 end]
    set foundMatch 0

    # Iterate through all faces the nodes were merged to. There should be a
    # matching face for the cell.
    foreach faceId $faceIdsMergedTo {
      set numNodes [lindex $face $numNodeRefIdsIdx]

      # Check if the current face has the same number of nodes as the search
      # entry.
      if {$numNodes > 0 && [llength $nodeIds] == $numNodes} {
        set faceNodeIds [lrange $faces($faceId) $firstNodeRefIdIdx \
          $lastNodeRefIdIdx]

        # Check if the search entry node ids matching with that one of the face.
        set foundMatch 1
        foreach nodeId $nodeIds {
          if {[lsearch -exact -integer $faceNodeIds $nodeId] < 0} {
            # Did not found node id. Therefore the current face is not the
            # matching one.
            set foundMatch 0
            break
          }
        }

        # Check if found a match.
        if {$foundMatch} {
          # A face that was merged has only one cell. It is either the first or
          # the second.
          if {[lindex $faces($faceId) $cell0RefIdIdx] < 0} {
            set faces($faceId) [lreplace $faces($faceId) $cell0RefIdIdx \
              $cell0RefIdIdx $cellId]
          } else {
            set faces($faceId) [lreplace $faces($faceId) $cell1RefIdIdx \
              $cell1RefIdIdx $cellId]
          }

          break
        }
      }
    }

    # Check again if found a match.
    if {!$foundMatch} {
      ::tcllogger::logError "::iswmesh::faces::updateFaceReferences" \
        [format "Couldn't find a matching face for the cell with the id %d!" \
        $cellId]
    }
  }
  ::tcllogger::log debug [format \
    "-> loop over cellsSearchForFaces needed %d milliseconds" \
    [expr {[clock clicks -milliseconds] - $ts2_1}]
  ]
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::faces::getFacesByZoneRefIds { zoneRefIds } {
  variable zoneRefIdIdx
  variable faces

  # Initialize zone faces lists list.
  set zoneFacesLists {}
  for {set i 0} {$i < [llength $zoneRefIds]} {incr i} {
    lappend zoneFacesLists {}
  }

  # Iterate through all faces to find the face of the zones.
  foreach {id face} [array get faces] {
    for {set i 0} {$i < [llength $zoneRefIds]} {incr i} {
      if {[lindex $face $zoneRefIdIdx] == [lindex $zoneRefIds $i]} {
        set zoneFaces [lindex $zoneFacesLists $i]
        lappend zoneFaces [concat $id $face]
        set zoneFacesLists [lreplace $zoneFacesLists $i $i $zoneFaces]
      }
    }
  }

  return $zoneFacesLists
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::faces::getNodeRefsOfFacesInZones { zoneRefIds } {
  variable zoneRefIdIdx
  variable numNodeRefIdsIdx
  variable firstNodeRefIdIdx
  variable faces

  # Initialize node references lists list.
  set nodeRefsLists [::utils::lInit [llength $zoneRefIds] {}]

  # Iterate through all faces to find the node references.
  foreach {id face} [array get faces] {
    set numNodes [lindex $face $numNodeRefIdsIdx]
    set lastNodeRefIdIdx [expr {$firstNodeRefIdIdx + $numNodes - 1}]

    for {set i 0} {$i < [llength $zoneRefIds]} {incr i} {
      if {[lindex $face $zoneRefIdIdx] == [lindex $zoneRefIds $i]} {
        set nodeRefsList [lindex $nodeRefsLists $i]
        set nodeRefIds [lrange $face $firstNodeRefIdIdx $lastNodeRefIdIdx]

        # Append the node ref ids unique, therefore not duplicate node ref ids
        # are in the list.
        foreach nodeRefId $nodeRefIds {
          ::utils::lAppendUnique nodeRefsList $nodeRefId
        }

        # Update the list in the node references lists list.
        lset nodeRefsLists $i $nodeRefsList
      }
    }
  }

  return $nodeRefsLists
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::faces::convertFacesToPreparsed { dataArray } {
  variable faceNumberIdx
  variable zoneRefIdIdx
  variable boundaryCondTypeIdx
  variable faceTypeIdx
  variable cell0RefIdIdx
  variable cell1RefIdIdx
  variable numNodeRefIdsIdx
  variable firstNodeRefIdIdx
  variable faces
  upvar $dataArray data

  set data(numFaces) [array size faces]
  array set zoneIdSectionMap {}
  set numSections 0
  set currSection -1
  set lastZoneId -1
  set faceSecBaseName ""

  foreach {id face} [array get faces] {
    set zoneRefId [lindex $face $zoneRefIdIdx]
    set zoneId [lindex $::iswmesh::zones::zones($zoneRefId) \
      $::iswmesh::zones::zoneIdIdx]
    set faceNum [lindex $face $faceNumberIdx]

    if {![info exists zoneIdSectionMap($zoneId)]} {
      set zoneIdSectionMap($zoneId) [list $numSections $data(numFaces) 0]
      set currSection $numSections
      incr numSections

      set faceSecBaseName "facesSection-$currSection"
      set data(${faceSecBaseName}-zoneId) $zoneId
      set data(${faceSecBaseName}-firstIndex) $data(numFaces)
      set data(${faceSecBaseName}-lastIndex) 0
      set data(${faceSecBaseName}-boundaryCondType) \
        [lindex $face $boundaryCondTypeIdx]
      set data(${faceSecBaseName}-faceType) [lindex $face $faceTypeIdx]
    } elseif {$lastZoneId != $zoneId} {
      set currSection [lindex $zoneIdSectionMap($zoneId) 0]
      set faceSecBaseName "facesSection-$currSection"
    }

    # Update firstIndex.
    set currFirst [lindex $zoneIdSectionMap($zoneId) 1]
    if {$faceNum < $currFirst} {
      set currFirst $faceNum
      set zoneIdSectionMap($zoneId) [lreplace $zoneIdSectionMap($zoneId) \
        1 1 $currFirst]
      set data(${faceSecBaseName}-firstIndex) $currFirst
    }

    # Update lastIndex.
    set currLast [lindex $zoneIdSectionMap($zoneId) 2]
    if {$faceNum > $currLast} {
      set currLast $faceNum
      set zoneIdSectionMap($zoneId) [lreplace $zoneIdSectionMap($zoneId) \
        2 2 $currLast]
      set data(${faceSecBaseName}-lastIndex) $currLast
    }

    # Add the face.
    set faceType $data(${faceSecBaseName}-faceType)
    set numNodes [lindex $face $numNodeRefIdsIdx]
    set lastNodeRefIdIdx [expr {$firstNodeRefIdIdx + $numNodes - 1}]

    # Resolve nodes.
    set nodeNumbers [::iswmesh::nodes::getNodeNumsByIds \
      [lrange $face $firstNodeRefIdIdx $lastNodeRefIdIdx]]

    # Resolve cells
    set cell0RefId [lindex $face $cell0RefIdIdx]
    set cell0Number 0

    if {$cell0RefId != -1} {
      set cell0Number [::iswmesh::cells::getCellNumsByIds $cell0RefId]
    }

    set cell1RefId [lindex $face $cell1RefIdIdx]
    set cell1Number 0

    if {$cell1RefId != -1} {
      set cell1Number [::iswmesh::cells::getCellNumsByIds $cell1RefId]
    }

    # Append converted face data.
    switch -exact -- $::fluentmesh::faceTypes($faceType) {
      mixed -
      polygonal {
        lappend data(${faceSecBaseName}-faces) [concat \
          $faceNum $numNodes $nodeNumbers $cell0Number $cell1Number]
      }
      linear -
      triangular -
      quadrilateral {
        lappend data(${faceSecBaseName}-faces) [concat \
          $faceNum $nodeNumbers $cell0Number $cell1Number]
      }
    }

    set lastZoneId $zoneId
  }

  for {set i 0} {$i < $numSections} {incr i} {
    set faceSecBaseName "facesSection-$i"
    set fs $data(${faceSecBaseName}-faces)
    set data(${faceSecBaseName}-faces) [lsort -integer -index 0 $fs]
  }

  set data(numFaceSections) $numSections
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::faces::printFaces { {fid -1} } {
  variable faces

  if {$fid == -1} {
    parray faces
  } else {
    ::utils::printArrayToFile $fid faces
  }
}


# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
