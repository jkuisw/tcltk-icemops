; Merges the given parts of the given mesh and saves the merged mesh to the
; given output file path.
;
; Parameters:
;   inputfile - Path to the valid fluent mesh file whose parts should be merged.
;     e.g.: "/path/to/fluent-mesh.msh" or "C:/path/to/dir/fluent-mesh.msh" or
;     "C:\\path\\to\\dir\\fluent-mesh.msh"
;   outputfile - Path to which the merged mesh should be saved. e.g.:
;     "/path/to/merged-fluent-mesh.msh" or
;     "C:/path/to/dir/merged-fluent-mesh.msh" or
;     "C:\\path\\to\\dir\\merged-fluent-mesh.msh"
;   parts-list - The list of parts to merge with their tolerance in the format:
;     (list
;       (list <tolerance> <part 1> <part 2>)
;       (list <tolerance> <part 3> <part 4>)
;       ...
;     )
;     The <tolerance> is a value which defines the maximum distance between
;     two nodes to get merged. The first part in the list entry will be deleted,
;     if all nodes of it are merged with the second one in the list entry. e.g.:
;     If all nodes of <part 1> above are merged with the nodes in <part 2>,
;     <part 1> will be deleted and <part 2> remains with the merged nodes.
(define (merge-parts-of-list inputfile outputfile parts-list)
  ; Read mesh file.
  (ti-menu-load-string (format #f "file read-mesh \"~a\"" inputfile))

  ; Merge the parts by calling for each list entry in the parts-list the
  ; merge-parts procedure.
  (map merge-parts parts-list)
  (ti-menu-load-string "boundary manage list")

  ; Write the merged mesh file.
  (if (file-exists? outputfile)
    ; File exists, therefore overwrite it ("yes" parameter).
    (ti-menu-load-string (format #f "file write-mesh \"~a\" yes" outputfile))
    ; File does not exist, therefore no "yes" parameter.
    (ti-menu-load-string (format #f "file write-mesh \"~a\"" outputfile))
  )

  ; Exit fluent.
  (ti-menu-load-string "exit")
)

; Merges the given parts.
;
; Parameters:
;   parts - A list in the format: (list <tolerance> <part 1> <part 2>)
;     Merges the nodes from the first part with the second if their distance is
;     smaller than the tolerance. If all nodes of the first part are merged, it
;     will be deleted.
(define (merge-parts parts)
  ; Merge the parts. Parameters of the "boundary merge-nodes" command are:
  ; (1) - List of zones to merge (first nodes of node pairs to merge).
  ; (2) - List of zones to merge (second nodes of node pairs to merge).
  ; (3) - Check only free nodes for first list of interfaces. -> no
  ; (4) - Check only free nodes for second list of interfaces. -> no
  ; (5) - Percent of shortest connected edge length tolerance. -> no, use
  ;       absolute value.
  ; (6) - The merge tolerance.
  (ti-menu-load-string (format #f "boundary merge-nodes (~a) (~a) no no no ~d\n"
    (list-ref parts 1) ; firts part name
    (list-ref parts 2) ; second part name
    (list-ref parts 0) ; tolerance
  ))
)
