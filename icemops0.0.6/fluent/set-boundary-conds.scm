; Sets the boundary conditions according to the given list and saves the mesh to
; the output file path.
;
; Parameters:
;   inputfile - Path to the valid fluent mesh file whose boundary conditions
;     should be set. e.g.: "/path/to/fluent-mesh.msh" or
;     "C:/path/to/dir/fluent-mesh.msh" or "C:\\path\\to\\dir\\fluent-mesh.msh"
;   outputfile - Path to which the mesh should be saved. e.g.:
;     "/path/to/merged-fluent-mesh.msh" or
;     "C:/path/to/dir/merged-fluent-mesh.msh" or
;     "C:\\path\\to\\dir\\merged-fluent-mesh.msh"
;   boundary-conditions - The list of boundary conditions in the format:
;     (list
;       (list <boundary condition name 1> (list
;         <part 1>
;         <part 2>
;         ...
;       ))
;       (list <boundary condition name 2> (list
;         <part 3>
;         <part 4>
;         ...
;       ))
;       ...
;     )
(define (set-boundary-conds inputfile outputfile boundary-conditions)
  ; Read mesh file.
  (ti-menu-load-string (format #f "file read-mesh \"~a\"" inputfile))

  ; Merge the parts by calling for each list entry in the parts-list the
  ; merge-parts procedure.
  (map set-boundary-condition-of-parts boundary-conditions)
  (ti-menu-load-string "boundary manage list")

  ; Write the merged mesh file.
  (if (file-exists? outputfile)
    ; File exists, therefore overwrite it ("yes" parameter).
    (ti-menu-load-string (format #f "file write-mesh \"~a\" yes" outputfile))
    ; File does not exist, therefore no "yes" parameter.
    (ti-menu-load-string (format #f "file write-mesh \"~a\"" outputfile))
  )

  ; Exit fluent.
  (ti-menu-load-string "exit")
)

; Sets the boundary condition of the given parts.
;
; Parameters:
;   boundary-condition - A list in the format:
;       (list <boundary condition name> (list
;         <part 1>
;         <part 2>
;         ...
;       ))
(define (set-boundary-condition-of-parts boundary-condition)
  ; Check if the requested boundary condition is a interface.
  (if (string-ci=? (list-ref boundary-condition 0) "interface") (begin
      ; If the requested boundary condition is "interface" set the parts
      ; boundary condition first to "wall" an than to interface. This is needed
      ; because it is not possible to change the boundary condition type from
      ; any to interface. But changeing form "wall" to "interface" is possible.

      ; Sets the boundary condition for the parts(zones). Parameters of the
      ; "boundary manage type" command are:
      ; (1) - List of part(zone) names).
      ; (2) - Boundary condition name.
      (ti-menu-load-string (format #f "boundary manage type ~a ~a\n"
        (list-ref boundary-condition 1) ; list of the part names
        "wall" ; boundary condition name
      ))

      ; Sets the boundary condition for the parts(zones). Parameters of the
      ; "boundary manage type" command are:
      ; (1) - List of part(zone) names).
      ; (2) - Boundary condition name.
      (ti-menu-load-string (format #f "boundary manage type ~a ~a\n"
        (list-ref boundary-condition 1) ; list of the part names
        (list-ref boundary-condition 0) ; boundary condition name
      ))
    ) (begin ; else
      ; Sets the boundary condition for the parts(zones). Parameters of the
      ; "boundary manage type" command are:
      ; (1) - List of part(zone) names).
      ; (2) - Boundary condition name.
      (ti-menu-load-string (format #f "boundary manage type ~a ~a\n"
        (list-ref boundary-condition 1) ; list of the part names
        (list-ref boundary-condition 0) ; boundary condition name
      ))
    )
  )
)
