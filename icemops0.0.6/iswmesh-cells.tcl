# package: icemops
# description:
#   This script file implements a custom mesh data model and operations.

# Initialize package.
source [file join [file dirname [info script]] "init-package.tcl"]

# Dependencies
package require Tcl 8.4
package require utils
package require tcllogger

# Define the namespace.
namespace eval ::iswmesh::cells {
  # Export commands.
  namespace export \
    initialize \
    createCells \
    addCells \
    updateCells \
    getAllCellsOfZone \
    getCellNumsByIds \
    setRefsByCellId \
    setRefsByCellNumber \
    deleteRefsByCellId \
    deleteRefsByCellNumber \
    convertCellsToPreparsed \
    printCells

  # Define namespace variables.
  variable freeIds {}
  variable maxId -1
  variable maxCellNumber 0

  # Data inidces for a cell entry of the cells array.
  variable referenceCountIdx 0
  variable cellNumberIdx 1
  variable zoneRefIdIdx 2
  variable cellTypeIdx 3
  variable elementTypeIdx 4
  variable cells
  array set cells {}

  variable cellNumIdMap
  array set cellNumIdMap {}
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
# { cellNumber zoneId cellType elementType }
# Returns:
proc ::iswmesh::cells::initialize { {newCells {}} } {
  variable freeIds
  variable maxId
  variable maxCellNumber
  variable cells
  variable cellNumIdMap

  array unset cells *
  array unset cellNumIdMap *
  set freeIds {}
  set maxId -1
  set maxCellNumber 0

  addCells $newCells
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
# { zoneId cellType elementType }
# Returns:
proc ::iswmesh::cells::createCells { newCells } {
  variable maxCellNumber

  foreach newCell $newCells {
    addCells [list [concat [incr maxCellNumber] $newCell]]
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
# { cellNumber zoneId cellType elementType }
# Returns:
proc ::iswmesh::cells::addCells { newCells } {
  variable freeIds
  variable maxId
  variable maxCellNumber
  variable cells
  variable cellNumIdMap
  set referenceCount 0

  foreach newCell $newCells {
    # Get a cell id.
    set newId [::iswmesh::getIdentifier freeIds maxId]

    # Get the cell values.
    set cellNumber [lindex $newCell 0]
    set zoneId [lindex $newCell 1]
    set cellType [lindex $newCell 2]
    set elementType [lindex $newCell 3]

    # Check if cell already exists.
    if {[info exists cellNumIdMap($cellNumber)]} {
      ::tcllogger::logError "::iswmesh::cells::addCells" \
        [format "Cell number %d already exists!" $cellNumber]
    }

    # Resolve zoneId and set a reference.
    set zoneRefId [lindex [::iswmesh::zones::setRefsByZoneId [list $zoneId]] 0]

    if {$cellNumber > $maxCellNumber} { set maxCellNumber $cellNumber }

    # Add the new cell to the cells array.
    set cells($newId) [list \
      $referenceCount \
      $cellNumber \
      $zoneRefId \
      $cellType \
      $elementType \
    ]

    # Add the cell to the cell number - id map array.
    set cellNumIdMap($cellNumber) $newId
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
# { id zoneId cellType elementType }
# Returns:
proc ::iswmesh::cells::updateCells { updatedCells } {
  variable zoneRefIdIdx
  variable elementTypeIdx
  variable cells

  foreach updatedCell $updatedCells {
    set id [lindex $updatedCell 0]

    if {![info exists cells($id)]} {
      ::tcllogger::logError "::iswmesh::cells::updateCells" \
        [format "Cell with id %d does not exist!" $id]
    }

    set newZoneRefId [lindex $updatedCell 1]
    set currentZoneRefId [lindex $cells($id) $zoneRefIdIdx]

    # Check if zone has changed.
    if {newZoneRefId != currentZoneRefId} {
      # Detected new zone, therefore remove reference to old one and set a
      # reference to the new one.
      ::iswmesh::zones::deleteRefsByZoneRefId [list $currentZoneRefId]
      ::iswmesh::zones::setRefsByZoneRefId [list $newZoneRefId]
    }

    # Update the cells data.
    set cells($id) [lreplace $cells($id) $zoneRefIdIdx $elementTypeIdx \
      [lrange $updatedCell 1 3]
    ]
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::cells::getAllCellsOfZone { zoneRefId } {
  variable zoneRefIdIdx
  variable cells
  set zoneCells {}

  foreach {id cell} [array get cells] {
    if {[lindex $cell $zoneRefIdIdx] == $zoneRefId} {
      lappend zoneCells $cell
    }
  }

  return $zoneCells
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::cells::getCellNumsByIds { ids } {
  variable cellNumberIdx
  variable cells
  set cellNums {}

  foreach id $ids {
    if {![info exists cells($id)]} {
      ::tcllogger::logError "::iswmesh::cells::getCellNumsByIds" \
      [format "Cell with id %d does not exist!" $id]
    }

    lappend cellNums [lindex $cells($id) $cellNumberIdx]
  }

  return $cellNums
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::cells::setRefsByCellId { cellIds } {
  variable referenceCountIdx
  variable cells

  foreach id $cellIds {
    if {![info exists cells($id)]} {
      ::tcllogger::logError "::iswmesh::cells::setRefsByCellId" \
        [format "Cell with id %d does not exist!" $id]
    }

    # Get cell from array.
    set cell $cells($id)

    # Increase reference count.
    set cell [lreplace $cell $referenceCountIdx $referenceCountIdx \
      [expr {[lindex $cell $referenceCountIdx] + 1}] \
    ]

    # Save updated cell in array.
    set cells($id) $cell
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::cells::setRefsByCellNumber { cellNumbers } {
  variable cellNumIdMap
  set ids {}

  foreach cellNumber $cellNumbers {
    if {![info exists cellNumIdMap($cellNumber)]} {
      ::tcllogger::logError "::iswmesh::cells::setRefsByCellNumber" \
        [format "Cell with number %d does not exist!" $cellNumber]
    }

    set id [list $cellNumIdMap($cellNumber)]
    setRefsByCellId $id
    lappend ids $id
  }

  return $ids
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::cells::deleteRefsByCellId { cellIds } {
  variable freeIds
  variable referenceCountIdx
  variable cellNumberIdx
  variable zoneRefIdIdx
  variable cells
  variable cellNumIdMap

  foreach id $cellIds {
    if {![info exists cells($id)]} {
      ::tcllogger::logError "::iswmesh::cells::deleteRefsByCellId" \
        [format "Cell with id %d does not exist!" $id]
    }

    # Get cell from array.
    set cell $cells($id)

    # Check if cell should be deleted (if there will be no reference to the
    # cell after this call).
    if {[lindex $cell $referenceCountIdx] <= 1} {
      # Delete the cell, because no one references it.
      set cellNumber [lindex $cell $cellNumberIdx]
      set zoneRefId [lindex $cell $zoneRefIdIdx]

      ::iswmesh::zones::deleteRefsByZoneRefId [list $zoneRefId]
      array unset cells $id
      array unset cellNumIdMap $cellNumber
      lappend freeIds $id
    } else {
      # Decrease reference count.
      set cell [lreplace $cell $referenceCountIdx $referenceCountIdx \
        [expr {[lindex $cell $referenceCountIdx] - 1}]
      ]

      # Save updated cell in array.
      set cells($id) $cell
    }
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::cells::deleteRefsByCellNumber { cellNumbers } {
  variable cellNumIdMap

  foreach cellNumber $cellNumbers {
    if {![info exists cellNumIdMap($cellNumber)]} {
      ::tcllogger::logError "::iswmesh::cells::deleteRefsByCellNumber" \
        [format "Cell with number %d does not exist!" $cellNumber]
    }

    deleteRefsByCellId [list $cellNumIdMap($cellNumber)]
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::cells::convertCellsToPreparsed { dataArray } {
  variable cellNumberIdx
  variable zoneRefIdIdx
  variable cellTypeIdx
  variable elementTypeIdx
  variable cells
  upvar $dataArray data

  set data(numCells) [array size cells]
  array set zoneIdSectionMap {}
  set numSections 0
  set currSection -1
  set lastZoneId -1
  set cellSecBaseName ""

  foreach {id cell} [array get cells] {
    set zoneRefId [lindex $cell $zoneRefIdIdx]
    set zoneId [lindex $::iswmesh::zones::zones($zoneRefId) \
      $::iswmesh::zones::zoneIdIdx]
    set cellNum [lindex $cell $cellNumberIdx]

    if {![info exists zoneIdSectionMap($zoneId)]} {
      set zoneIdSectionMap($zoneId) [list $numSections $data(numCells) 0]
      set currSection $numSections
      incr numSections

      set cellSecBaseName "cellsSection-$currSection"
      set data(${cellSecBaseName}-zoneId) $zoneId
      set data(${cellSecBaseName}-firstIndex) $data(numCells)
      set data(${cellSecBaseName}-lastIndex) 0
      set data(${cellSecBaseName}-cellsType) [lindex $cell $cellTypeIdx]
      set data(${cellSecBaseName}-elementType) [lindex $cell $elementTypeIdx]
    } elseif {$lastZoneId != $zoneId} {
      set currSection [lindex $zoneIdSectionMap($zoneId) 0]
      set cellSecBaseName "cellsSection-$currSection"
    }

    # Update firstIndex.
    set currFirst [lindex $zoneIdSectionMap($zoneId) 1]
    if {$cellNum < $currFirst} {
      set currFirst $cellNum
      set zoneIdSectionMap($zoneId) [lreplace $zoneIdSectionMap($zoneId) \
        1 1 $currFirst]
      set data(${cellSecBaseName}-firstIndex) $currFirst
    }

    # Update lastIndex.
    set currLast [lindex $zoneIdSectionMap($zoneId) 2]
    if {$cellNum > $currLast} {
      set currLast $cellNum
      set zoneIdSectionMap($zoneId) [lreplace $zoneIdSectionMap($zoneId) \
        2 2 $currLast]
      set data(${cellSecBaseName}-lastIndex) $currLast
    }

    # Add the cell.
    lappend data(${cellSecBaseName}-cells) $cellNum

    set lastZoneId $zoneId
  }

  for {set i 0} {$i < $numSections} {incr i} {
    set cellSecBaseName "cellsSection-$i"
    set cs $data(${cellSecBaseName}-cells)
    set data(${cellSecBaseName}-cells) [lsort -integer $cs]
  }

  set data(numCellSections) $numSections
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::cells::printCells { {fid -1} } {
  variable cells

  if {$fid == -1} {
    parray cells
  } else {
    ::utils::printArrayToFile $fid cells
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
