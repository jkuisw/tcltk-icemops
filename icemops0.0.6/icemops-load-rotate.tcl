# package: icemops
# description:
#   This script includes functions for loading and rotating a ANSYS ICEM CFD
#   mesh.

# Initialize package.
source [file join [file dirname [info script]] "init-package.tcl"]

# Dependencies
package require Tcl 8.4
package require utils
package require tcllogger

# Define the namespace.
namespace eval ::icemops {
  # Export commands.
  namespace export \
    loadRotateComponent3D \
    loadUnsMesh3D

  # Define namespace variables.
  # Nothing for now.
}

# ------------------------------------------------------------------------------
# This function loads and rotates components of an ICEM CFD project selected by
# their family (= part).
#
# Parameters:
#   path - Path to the unstructured mesh file (*.uns).
#   shortName - The short name of the component to be rotated. This will be used
#     in variable and part names, therefore the short name must be a valid
#     variable name.
#   longName - The long name of the component to be rotated. This will be used
#      for logging and error messages.
#   partFamilies - A list of families of the component. The list must have at
#     least one entry and must have the same length as the partNames list. The
#     familiy names can be regular expressions that will be matched with the
#     existing family (part) names.
#   partNames - A list of part names, will be used for variables and part names,
#     therefore the part names must be valid variable names. These names will be
#     used to distinguish the different families (= parts) of a component. The
#     list must have at least one entry and must have the same length as the
#     partFamilies list.
#   rotateAxis - A three dimensional vector (list with three entries) which
#     defines the rotation axis.
#   rotateCenter - A three dimensional vector (list with three entries) which
#     defines the rotation center.
#   deltaAngle - The angle between two rotated components, must be smaller than
#     the endAngle.
#   endAngle - With the end angle the number of copied and rotated components
#     will be calculated: number of components = endAngle / deltaAngle
#     The endAngle is a relative angle starting from the startAngle.
#   startAngle - [default 0] The angle from which to start the rotation and is
#     an offset relative from the current position.
#   isFirst - [default false] If this is the first mesh loaded into an empty
#     project. The procedure will execute some further commands if so.
#   replace - [default false] If the loaded component should replace an existing
#     mesh.
proc ::icemops::loadRotateComponent3D {
  shortName
  longName
  path
  partFamilies
  partNames
  rotateAxis
  rotateCenter
  deltaAngle
  endAngle
  {startAngle 0}
  {isFirst false}
  {replace false}
} {
  # Check if mesh file exists.
  if {[file exists $path] == false} {
    ::tcllogger::logError "::icemops::loadRotateComponent3D" \
      "The mesh file \"$path\" does not exist!"
  }

  # Check part list parameters.
  if {[llength $partFamilies] <= 0} {
    ::tcllogger::logError "::icemops::loadRotateComponent3D" \
      "Need at least one part family to proceed."
  } elseif {[llength $partNames] <= 0} {
    ::tcllogger::logError "::icemops::loadRotateComponent3D" \
      "Need at least one part name to proceed."
  } elseif {[llength $partFamilies] != [llength $partNames]} {
    ::tcllogger::logError "::icemops::loadRotateComponent3D" \
      "partFamilies and partNames must be lists with same length!"
  }

  # Check angle value parameters.
  if {$deltaAngle > $endAngle} {
    ::tcllogger::logError "::icemops::loadRotateComponent3D" \
      "deltaAngle must be smaller than endAngle!"
  }

  # Check rotate axis parameter.
  if {[llength $rotateAxis] != 3} {
    ::tcllogger::logError "::icemops::loadRotateComponent3D" \
      "rotateAxis must have 3 components (3D vector)!"
  } elseif {[llength $rotateCenter] != 3} {
    ::tcllogger::logError "::icemops::loadRotateComponent3D" \
      "rotateCenter must have 3 components (3D vector)!"
  }

  # Calculate number of components.
  set numComponents [expr {$endAngle / $deltaAngle}]

  # Start loading and rotating.
  for {set i 0} {$i < $numComponents} {incr i} {
    set angle [expr {($i * $deltaAngle) + $startAngle}]
    ::tcllogger::log info [::utils::indentText "(*) Processing $longName $i"]

    # Load next component.
    loadUnsMesh3D $longName $path $isFirst $replace

    if {$i == 0} {
      set isFirst false
      set replace false
    }

    # Resolve part names.
    if {$i == 0} {
      set partFamilies [resolveParts $partFamilies]
    }

    # Rename families of next component.
    set newPartNames [renameFamilies $partFamilies $partNames $shortName $i]

    # Rotate parts of component.
    rotateParts $newPartNames $angle $rotateAxis $rotateCenter

    ::tcllogger::log info [::utils::indentText \
      "-> Finished loading and rotating $longName $i\n" 6]
  }
}

# ------------------------------------------------------------------------------
# Loads an unstructured mesh.
#
# Parameters:
#   compName - The name of the mesh component which will be loaded. Used for
#     logging output only.
#   path - The path to the mesh file to load.
#   isFirst - [default false] If this is the first mesh loaded into an empty
#     project. The procedure will execute some further commands if so.
#   replace - [default false] If the loaded component should replace an existing
#     mesh.
#   formatOutput - [default true] If the logging output shoule be formatted as
#    list element: tab and "->" as list entry marker.
proc ::icemops::loadUnsMesh3D {
  compName
  path
  {isFirst false}
  {replace false}
  {formatOutput true}
} {
  # Check if mesh file exists.
  if {[file exists $path] == false} {
    ::tcllogger::logError "::icemops::loadUnsMesh" \
      "The mesh file \"$path\" does not exist!"
  }

  # When replace is true, delete the existing mesh.
  if {$replace == true} {
    if {$formatOutput == true} {
      ::tcllogger::log info [::utils::indentText "-> delete existing mesh" 6]
    } else {
      ::tcllogger::log info "delete existing mesh"
    }
    ic_unload_mesh
    ic_uns_diag_reset_degen_min_max
    ic_delete_empty_parts
  }

  # load the unstructured mesh.
  if {$formatOutput == true} {
    ::tcllogger::log info [::utils::indentText \
      "-> import \"$compName\" unstructured mesh" 6]
  } else {
    ::tcllogger::log info "import \"$compName\" unstructured mesh"
  }
  ic_uns_load "$path" 3 0 {} 2

  if {$isFirst == true} {
    ic_boco_solver
    ic_uns_update_family_type visible {__all__} {__all__} update 0
    ic_boco_clear_icons
    ic_csystem_display all 0
    ic_csystem_set_current global
    ic_boco_nastran_csystem reset
    ic_uns_update_family_type visible {__all__} {__all__} update 0
    ic_uns_diag_reset_degen_min_max
  }
}

# ------------------------------------------------------------------------------
# Rotate the parts in the given part list by the given angle, rotation axis and
# center.
# Note: All parts in the given list should belong to one mesh component (loaded
#   from one file).
#
# Parameters:
#   partNames - A list of parts (families) to rotate.
#   angle - The rotation angle.
#   rotateAxis - The rotation axis, a 3 dimensional vector.
#   rotateCenter - The rotation center, a 3 dimensional vector.
proc ::icemops::rotateParts { partNames angle rotateAxis rotateCenter } {
  # Create a temporary subset.
  set tmpSubset [ic_uns_subset_create]

  # Add part to subset.
  ic_uns_subset_add_families_and_types $tmpSubset $partNames [ic_uns_list_types]

  # Rotate the elements in the subset.
  ic_start_bigunsop 1
  ic_uns_move_elements $tmpSubset rotate $angle \
    rotate_axis $rotateAxis \
    cent $rotateCenter
  ic_finish_bigunsop

  # Delete temporary subset.
  ic_uns_subset_delete $tmpSubset

  ::tcllogger::log debug \
    [::utils::indentText "-> parts \"$partNames\" rotated by $angle°" 6]
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
