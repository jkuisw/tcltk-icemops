# name: convertFluentToUns.tcl
# description: Sript for converting a fluent *.msh mesh file to a icemcfd *.uns
#   mesh file. This script is normally be called by the icemops command
#   "::icemops::convertFluentToUns". The third argument must be the path to the
#   fluent *.msh file, the fourth argument must be a path to the newly created
#   *.uns mesh file and the fifth parameter must be to log level of the
#   tcllogger. All other arguments are irrelevant.

# ******************************************************************************
# Set library paths.
global auto_path

# Paths to package directories relative from this script.
set paths [list \
  [file join ".." ".."] \
  [file join ".." ".." "submodules" "tcltk-utils"] \
  [file join ".." ".." "submodules" "tcltk-logger"] \
]

# Add the package paths to the global auto_path variable.
foreach path $paths {
  set libpath [file normalize [file join [file dirname [info script]] $path]]
  if {[lsearch $auto_path $libpath] == -1} {lappend auto_path "$libpath"}
}

# ******************************************************************************
# Define dependencies and load packages.
package require Tcl 8.4
package require icemops
package require tcllogger

# ******************************************************************************
# parse the arguments
set sourceFile [lindex $argv 3]
set targetFile [lindex $argv 4]
set logLevel [lindex $argv 5]

# ******************************************************************************
# configure the tcllogger
::tcllogger::configure logger icem
::tcllogger::configure logLevel $logLevel

# ******************************************************************************
# start the conversion
::tcllogger::log debug "Import the fluent mesh \"$sourceFile\"."
::icemops::importFluentMesh $sourceFile

::tcllogger::log debug "Save the imported mesh as \"$targetFile\"."
ic_save_unstruct $targetFile

if {![file exists $targetFile]} {
  ::tcllogger::logError "convertFluentToUns" \
    "Failed to convert mesh file \"$sourceFile\"!"
}

::tcllogger::log info "Successfully converted mesh to \"$targetFile\"."

# Exit the icemcfd instance.
exit
