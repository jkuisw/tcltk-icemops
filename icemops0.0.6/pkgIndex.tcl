# Tcl package index file, version 1.1
# This file is generated by the "pkg_mkIndex" command
# and sourced either when an application starts up or
# by a "package unknown" script.  It invokes the
# "package ifneeded" command to set up package-related
# information so that packages will be loaded automatically
# in response to "package require" commands.  When this
# script is sourced, the variable $dir must contain the
# full path name of this file's directory.

package ifneeded icemops 0.0.6 [list source [file join $dir fluent-mesh.tcl]]\n[list source [file join $dir icemops-load-rotate.tcl]]\n[list source [file join $dir icemops-merge.tcl]]\n[list source [file join $dir icemops-utils.tcl]]\n[list source [file join $dir init-package.tcl]]\n[list source [file join $dir iswmesh-cells.tcl]]\n[list source [file join $dir iswmesh-faces.tcl]]\n[list source [file join $dir iswmesh-nodes.tcl]]\n[list source [file join $dir iswmesh-zones.tcl]]\n[list source [file join $dir iswmesh.tcl]]
