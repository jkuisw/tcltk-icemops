# package: icemops
# description:
#   TODO

# Initialize package.
source [file join [file dirname [info script]] "init-package.tcl"]

# Dependencies
package require Tcl 8.4
package require utils
package require tcllogger

# Define the namespace.
namespace eval ::iswmesh::nodes {
  # Export commands.
  namespace export \
    initialize \
    createNodes \
    addNodes \
    updateNodes \
    getAllNodesOfZone \
    getNodeById \
    getNodeNumsByIds \
    mergeNodes \
    setRefsByNodeId \
    setRefsByNodeNumber \
    deleteRefsByNodeId \
    deleteRefsByNodeNumber \
    checkRefsById \
    convertNodesToPreparsed \
    printNodes

  # Define namespace variables.
  variable freeIds {}
  variable maxId -1
  variable maxNodeNumber 0

  # -------------------------------------------------
  # Data indices for a node entry of the nodes array.
  # Node data list index where the node references are stored.
  variable referencesIdx 0
  # Node data list index where the node number is stored.
  variable nodeNumberIdx 1
  # Node data list index where the list of node coordinates is stored.
  variable coordinatesIdx 2
  # Node data list index where the zone reference id is stored.
  variable zoneRefIdIdx 3
  # Node data list index where the node type is stored.
  variable nodeTypeIdx 4
  # Node data list index where the status value/list is stored.
  variable statusIdx 5

  # ----------------
  # The nodes array.
  variable nodes
  array set nodes {}

  # --------------------------------------------------------------------
  # List indices for a node reference entry in the node references list.
  # The references list index where the reference id of the face which has a
  # registered reference to this node is stored.
  variable nrFaceIdIdx 0
  # The references list index where the relative index of the node reference id
  # in the corresponding face data list is stored. (e.g.: 0 -> first node of
  # face, 1 -> second node of face, 2 -> third node of face, ...)
  variable nrNodePosIdx 1

  # ----------------------------------------------------------------
  # Array which maps the node number (key) with the node id (value).
  variable nodeNumIdMap
  array set nodeNumIdMap {}
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
# { nodeNumber coordinates zoneId nodeType }
# Returns:
proc ::iswmesh::nodes::initialize { {newNodes {}} } {
  variable freeIds
  variable maxId
  variable maxNodeNumber
  variable nodes
  variable nodeNumIdMap

  array unset nodes *
  array unset nodeNumIdMap *
  set freeIds {}
  set maxId -1
  set maxNodeNumber 0

  addNodes $newNodes
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# { coordinates zoneId nodeType }
# Returns:
proc ::iswmesh::nodes:createNodes { newNodes } {
  variable maxNodeNumber

  foreach newNode $newNodes {
    addNodes [list [concat [incr maxNodeNumber] $newNode]]
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# { nodeNumber coordinatesList zoneId nodeType }
# Returns:
proc ::iswmesh::nodes::addNodes { newNodes } {
  variable freeIds
  variable maxId
  variable maxNodeNumber
  variable nodes
  variable nodeNumIdMap
  set references {}

  foreach newNode $newNodes {
    # Get a node id.
    set newId [::iswmesh::getIdentifier freeIds maxId]

    # Get the node values.
    set nodeNumber [lindex $newNode 0]
    set coordinates [lindex $newNode 1]
    set zoneId [lindex $newNode 2]
    set nodeType [lindex $newNode 3]
    set status 0

    # Check if node already exists.
    if {[info exists nodeNumIdMap($nodeNumber)]} {
      ::tcllogger::logError "::iswmesh::nodes::addNodes" \
        [format "Node number %d already exists!" $nodeNumber]
    }

    # Resolve zoneId and set a reference.
    if {![::iswmesh::zones::doesZoneExist $zoneId]} {
      # Create special zone for nodes.
      ::iswmesh::zones::addZones [list [list $zoneId "NODES" "nodes"]]
    }
    set zoneRefId [lindex [::iswmesh::zones::setRefsByZoneId [list $zoneId]] 0]

    if {$nodeNumber > $maxNodeNumber} { set maxNodeNumber $nodeNumber }

    # Add the new node to the nodes array.
    set nodes($newId) [list \
      $references \
      $nodeNumber \
      $coordinates \
      $zoneRefId \
      $nodeType \
      $status \
    ]

    # Add the node to the node number - id map array.
    set nodeNumIdMap($nodeNumber) $newId
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
# { id coordinates zoneRefId nodeType }
# Returns:
proc ::iswmesh::nodes::updateNodes { updatedNodes } {
  variable coordinatesIdx
  variable zoneRefIdIdx
  variable nodeTypeIdx
  variable statusIdx
  variable nodes

  foreach updatedNode $updateNodes {
    set id [lindex $updatedNode 0]

    if {![info exists nodes($id)]} {
      ::tcllogger::logError "::iswmesh::nodes::updateNodes" \
        [format "Node with id %d does not exist!" $id]
    }

    if {[lindex [lindex $nodes($id) $statusIdx] 0] == "mergedWith"} {
      ::tcllogger::log warn [format \
        "Node with id %d is merged with node id %d. %s" $id \
        [lindex [lindex $nodes($id) $statusIdx] 1] \
        "Therefore update will be ignored."]
    } else {
      set newZoneRefId [lindex $updatedNode 2]
      set currentZoneRefId [lindex $nodes($id) $zoneRefIdIdx]

      # Check if zone has changed.
      if {newZoneRefId != currentZoneRefId} {
        # Detected new zone, therefore remove reference to old one and set a
        # reference to the new one.
        ::iswmesh::zones::deleteRefsByZoneRefId [list $currentZoneRefId]
        ::iswmesh::zones::setRefsByZoneRefId [list $newZoneRefId]
      }

      # Update the nodes data.
      set nodes($id) [lreplace $nodes($id) $coordinatesIdx $nodeTypeIdx \
        [lrange $updateNode 1 3]
      ]
    }
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
#   { id refs nums coordinates zoneRefId nodeType status }
proc ::iswmesh::nodes::getAllNodesOfZones { zoneRefIds } {
  variable zoneRefIdIdx
  variable nodes

  # Initialize zone nodes lists list.
  set zoneNodesLists {}
  for {set i 0} {$i < [llength $zoneRefIds]} {incr i} {
    lappend zoneNodesLists {}
  }

  # Iterate through all nodes to find the nodes of the zones.
  foreach {id node} [array get nodes] {
    for {set i 0} {$i < [llength $zoneRefIds]} {incr i} {
      if {[lindex $node $zoneRefIdIdx] == [lindex $zoneRefIds $i]} {
        set zoneNodes [lindex $zoneNodesLists $i]
        lappend zoneNodes [concat $id $node]
        set zoneNodesLists [lreplace $zoneNodesLists $i $i $zoneNodes]
      }
    }
  }

  return $zoneNodesLists
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::nodes::getNodeById { id } {
  variable nodes

  if {![info exists nodes($id)]} {
    ::tcllogger::logError "::iswmesh::nodes::getNodeById" \
      [format "Node with id %d does not exist!" $id]
  }

  return $nodes($id)
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::nodes::getNodeNumsByIds { ids } {
  variable nodeNumberIdx
  variable nodes
  set nodeNums {}

  foreach id $ids {
    if {![info exists nodes($id)]} {
      ::tcllogger::logError "::iswmesh::nodes::getNodeNumsByIds" \
      [format "Node with id %d does not exist!" $id]
    }

    lappend nodeNums [lindex $nodes($id) $nodeNumberIdx]
  }

  return $nodeNums
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
# { {nodeId1 nodeId2} {nodeId3 nodeId4} }
# Returns:
proc ::iswmesh::nodes::mergeNodes { nodeMergePairs } {
  variable referencesIdx
  variable statusIdx
  variable nodes

  # Array of faces which references to nodes that are merged and therefore will
  # be removed. Most of this faces will be deleted, but not all. Only that faces
  # of which all nodes were merged.
  #array set facesWithMergedNodes {}

  # Array of faces which references to that nodes which the other nodes were
  # merged to.
  #array set facesMergedTo {}

  foreach pair $nodeMergePairs {
    # The node to which the node below will be merged to.
    set nodeToMergeWith [lindex $pair 0]

    # The node which will be merged to that one above. Will be deleted when all
    # references are removed.
    set nodeToMerge [lindex $pair 1]

    if {![info exists nodes($nodeToMergeWith)]} {
      ::tcllogger::logError "::iswmesh::nodes::mergeNodes" \
        [format "Node with id %d does not exist!" $id]
    }

    if {![info exists nodes($nodeToMerge)]} {
      ::tcllogger::logError "::iswmesh::nodes::mergeNodes" \
        [format "Node with id %d does not exist!" $id]
    }

    # nodeToMergeWith <--- nodeToMerge
    set nodes($nodeToMerge) [lreplace $nodes($nodeToMerge) $statusIdx \
      $statusIdx [list "mergedWith" $nodeToMergeWith]]

    # Update faces.
    foreach reference [lindex $nodes($nodeToMerge) $referencesIdx] {
      set faceId [lindex $reference 0]
      set nodePos [lindex $reference 1]
      ::iswmesh::faces::updateMergedNodeOfFace $faceId $nodePos \
        $nodeToMerge $nodeToMergeWith
    }

    # Add face ids with merged nodes to array.
    #foreach faceId [lindex $nodes($nodeToMerge) $referencesIdx] {
    #  if {![info exists affectedFaces($faceId)]} {
    #    set facesWithMergedNodes($faceId) $faceId
    #  }
    #}

    # Add face ids with the nodes the other nodes were merged to, to the array.
    #foreach faceId [lindex $nodes($nodeToMergeWith) $referencesIdx] {
    #  if {![info exists affectedFaces($faceId)]} {
    #    set facesMergedTo($faceId) $faceId
    #  }
    #}
  }

  #return [list [array names facesWithMergedNodes] [array names facesMergedTo]]
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::nodes::setRefsByNodeId { faceIdNodeIdPosLists } {
  variable referencesIdx
  variable nodes
  variable nrFaceIdIdx
  variable nrNodePosIdx

  foreach lst $faceIdNodeIdPosLists {
    set faceId [lindex $lst 0]
    set nodeId [lindex $lst 1]
    set nodePosition [lindex $lst 2]

    if {![info exists nodes($nodeId)]} {
      ::tcllogger::logError "::iswmesh::nodes::setRefsByNodeId" \
        [format "Node with id %d does not exist!" $nodeId]
    }

    # Get node from array.
    set node $nodes($nodeId)

    # Set reference to face.
    set refsList [lindex $node $referencesIdx]
    lappend refsList [list $faceId $nodePosition]
    set node [lreplace $node $referencesIdx $referencesIdx $refsList]

    # Save updated node in array.
    set nodes($nodeId) $node
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns: node ids
proc ::iswmesh::nodes::setRefsByNodeNumber { faceIdNodeNumPosLists } {
  variable nodeNumIdMap
  set nodeIds {}

  foreach lst $faceIdNodeNumPosLists {
    set faceId [lindex $lst 0]
    set nodeNumber [lindex $lst 1]
    set nodePosition [lindex $lst 2]

    if {![info exists nodeNumIdMap($nodeNumber)]} {
      ::tcllogger::logError "::iswmesh::nodes::setRefsByNodeNumber" \
        [format "Node with number %d does not exist!" $nodeNumber]
    }

    set nodeId [list $nodeNumIdMap($nodeNumber)]
    setRefsByNodeId [list [list $faceId $nodeId $nodePosition]]
    lappend nodeIds $nodeId
  }

  return $nodeIds
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::nodes::deleteRefsByNodeId { faceIdNodeIdPairs } {
  variable freeIds
  variable referencesIdx
  variable nodeNumberIdx
  variable zoneRefIdIdx
  variable nodes
  variable nodeNumIdMap

  foreach pair $faceIdNodeIdPairs {
    set faceId [lindex $pair 0]
    set nodeId [lindex $pair 1]

    if {![info exists nodes($nodeId)]} {
      ::tcllogger::logError "::iswmesh::nodes::deleteRefsByNodeId" \
        [format "Node with id %d does not exist!" $nodeId]
    }

    # Get node from array.
    set node $nodes($nodeId)

    # Check if node should be deleted (if there will be no reference to the
    # node after this call).
    if {[llength [lindex $node $referencesIdx]] <= 1} {
      # Delete the node, because no one references it any more.
      set nodeNumber [lindex $node $nodeNumberIdx]
      set zoneRefId [lindex $node $zoneRefIdIdx]

      ::iswmesh::zones::deleteRefsByZoneRefId [list $zoneRefId]
      array unset nodes $nodeId
      array unset nodeNumIdMap $nodeNumber
      lappend freeIds $nodeId
    } else {
      # Remove reference to face.
      set node [lreplace $node $referencesIdx $referencesIdx \
        [lsearch -all -inline -not \
          [lindex $node $referencesIdx] \
          [list $faceId *] \
        ]
      ]

      # Save updated node in array.
      set nodes($nodeId) $node
    }
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::nodes::deleteRefsByNodeNumber { faceIdNodeNumberPairs } {
  variable nodeNumIdMap

  foreach pair $faceIdNodeNumberPairs {
    set faceId [lindex $pair 0]
    set nodeNumber [lindex $pair 1]

    if {![info exists nodeNumIdMap($nodeNumber)]} {
      ::tcllogger::logError "::iswmesh::nodes::deleteRefsByNodeNumber" \
        [format "Node with number %d does not exist!" $nodeNumber]
    }

    deleteRefsByNodeId [list [list $faceId $nodeNumIdMap($nodeNumber)]]
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
# Returns:
# nodeId < 0 ... node ref not changed
# { numChanged nodeId1 nodeId2 ... }
proc ::iswmesh::nodes::checkRefsById { faceId nodeIds } {
  variable statusIdx
  variable nodes
  set numChanged 0
  set newIds {}

  foreach nodeId $nodeIds {
    # Check if node exists.
    if {![info exists nodes($nodeId)]} {
      ::tcllogger::logError "::iswmesh::nodes::checkRefsById" \
        [format "Node with id %d does not exist!" $nodeId]
    }

    set status [lindex $nodes($nodeId) $statusIdx]
    if {[llength $status] > 1 && [lindex $status 0] == "mergedWith"} {
      lappend newIds [lindex $status 1]
      incr numChanged
      deleteRefsByNodeId [list [list $faceId $nodeId]]
    } else {
      lappend newIds -1
    }
  }

  return [concat $numChanged $newIds]
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::nodes::convertNodesToPreparsed { dataArray } {
  variable nodeNumberIdx
  variable coordinatesIdx
  variable zoneRefIdIdx
  variable nodeTypeIdx
  variable nodes
  upvar $dataArray data

  set data(numNodes) [array size nodes]
  array set zoneIdSectionMap {}
  set numSections 0
  set currSection -1
  set lastZoneId -1
  set nodeSecBaseName ""

  foreach {id node} [array get nodes] {
    set zoneRefId [lindex $node $zoneRefIdIdx]
    set zoneId [lindex $::iswmesh::zones::zones($zoneRefId) \
      $::iswmesh::zones::zoneIdIdx]
    set nodeNum [lindex $node $nodeNumberIdx]

    if {![info exists zoneIdSectionMap($zoneId)]} {
      set zoneIdSectionMap($zoneId) [list $numSections $data(numNodes) 0]
      set currSection $numSections
      incr numSections

      set nodeSecBaseName "nodesSection-$currSection"
      set data(${nodeSecBaseName}-zoneId) $zoneId
      set data(${nodeSecBaseName}-firstIndex) $data(numNodes)
      set data(${nodeSecBaseName}-lastIndex) 0
      set data(${nodeSecBaseName}-nodesType) [lindex $node $nodeTypeIdx]
      set data(${nodeSecBaseName}-nodesDimension) $data(dimension)
    } elseif {$lastZoneId != $zoneId} {
      set currSection [lindex $zoneIdSectionMap($zoneId) 0]
      set nodeSecBaseName "nodesSection-$currSection"
    }

    # Update firstIndex.
    set currFirst [lindex $zoneIdSectionMap($zoneId) 1]
    if {$nodeNum < $currFirst} {
      set currFirst $nodeNum
      set zoneIdSectionMap($zoneId) [lreplace $zoneIdSectionMap($zoneId) \
        1 1 $currFirst]
      set data(${nodeSecBaseName}-firstIndex) $currFirst
    }

    # Update lastIndex.
    set currLast [lindex $zoneIdSectionMap($zoneId) 2]
    if {$nodeNum > $currLast} {
      set currLast $nodeNum
      set zoneIdSectionMap($zoneId) [lreplace $zoneIdSectionMap($zoneId) \
        2 2 $currLast]
      set data(${nodeSecBaseName}-lastIndex) $currLast
    }

    # Add the node.
    lappend data(${nodeSecBaseName}-nodes) [concat \
      $nodeNum [lindex $node $coordinatesIdx]]

    set lastZoneId $zoneId
  }

  for {set i 0} {$i < $numSections} {incr i} {
    set nodeSecBaseName "nodesSection-$i"
    set ns $data(${nodeSecBaseName}-nodes)
    set data(${nodeSecBaseName}-nodes) [lsort -integer -index 0 $ns]
  }

  set data(numNodeSections) $numSections
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::nodes::printNodes { {fid -1} } {
  variable nodes

  if {$fid == -1} {
    parray nodes
  } else {
    ::utils::printArrayToFile $fid nodes
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
