# package: icemops
# description:
#   This script file implements a custom mesh data model and operations.

# Initialize package.
source [file join [file dirname [info script]] "init-package.tcl"]

# Dependencies
package require Tcl 8.4
package require utils
package require tcllogger

# Define the namespace.
namespace eval ::iswmesh::zones {
  # Export commands.
  namespace export \
    initialize \
    createZones \
    addZones \
    updateZones \
    doesZoneExist \
    getRefIdsByName \
    setRefsByZoneRefId \
    setRefsByZoneId \
    deleteRefsByZoneRefId \
    deleteRefsByZoneId \
    convertZonesToPreparsed \
    printZones

  # Define namespace variables.
  variable freeRefIds {}
  variable maxRefId -1
  variable maxZoneId 0

  # Data inidces for a zone entry of the zones array.
  variable referenceCountIdx 0
  variable zoneIdIdx 1
  variable zoneNameIdx 2
  variable zoneTypeIdx 3
  variable zones
  array set zones {}

  variable zoneIdRefIdMap
  array set zoneIdRefIdMap {}
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
# { zoneId zoneName zoneType }
# Returns:
proc ::iswmesh::zones::initialize { {newZones {}} } {
  variable freeRefIds
  variable maxRefId
  variable maxZoneId
  variable zones
  variable zoneIdRefIdMap

  array unset zones *
  array unset zoneIdRefIdMap *
  set freeRefIds {}
  set maxRefId -1
  set maxZoneId 0

  addZones $newZones
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
# { zoneName zoneType }
# Returns:
proc ::iswmesh::zones::createZones { newZones } {
  variable maxZoneId

  foreach newZone $newZones {
    addZones [list [concat [incr maxZoneId] $newZone]]
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
# { zoneId zoneName zoneType }
# Returns:
proc ::iswmesh::zones::addZones { newZones } {
  variable freeRefIds
  variable maxRefId
  variable maxZoneId
  variable zones
  variable zoneIdRefIdMap
  set referenceCount 0

  foreach newZone $newZones {
    # Get a zone reference id.
    set newRefId [::iswmesh::getIdentifier freeRefIds maxRefId]

    # Get the zone values.
    set zoneId [lindex $newZone 0]
    set zoneName [lindex $newZone 1]
    set zoneType [lindex $newZone 2]

    # Check if zone already exists.
    if {[info exists zoneIdRefIdMap($zoneId)]} {
      ::tcllogger::logError "::iswmesh::zones::addZones" \
        [format "Zone with id %d already exists!" $zoneId]
    }

    if {$zoneId > $maxZoneId} { set maxZoneId $zoneId }

    # Add the new zone to the zones array.
    set zones($newRefId) [list \
      $referenceCount \
      $zoneId \
      $zoneName \
      $zoneType \
    ]

    # Add the zone to the zone id - reference id map array.
    set zoneIdRefIdMap($zoneId) $newRefId
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
# { refId zoneId zoneName zoneType }
# Returns:
proc ::iswmesh::zones::updateZones { updatedZones } {
  variable zoneIdIdx
  variable zoneTypeIdx
  variable zones

  foreach updatedZone $updatedZones {
    set refId [lindex $updatedZone 0]

    if {![info exists zones($refId)]} {
      ::tcllogger::logError "::iswmesh::zones::updateZones" \
        [format "Zone with reference id %d does not exist!" $refId]
    }

    set zones($refId) [lreplace $zones($refId) $zoneIdIdx $zoneTypeIdx \
      [lrange $updatedZone 1 3]
    ]
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::zones::doesZoneExist { {zoneId 0} {zoneRefId -1} } {
  variable zones
  variable zoneIdRefIdMap

  if {$zoneId > 0} {
    if {[info exists zoneIdRefIdMap($zoneId)]} { return 1 }
  } elseif {$zoneRefId >= 0} {
    if {[info exists zones($zoneRefId)]} { return 1 }
  }

  return 0
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::zones::getRefIdsByName { zoneNames } {
  variable zoneNameIdx
  variable zones
  set refIds {}

  foreach zoneName $zoneNames {
    foreach {refId zone} [array get zones] {
      if {[string match $zoneName [lindex $zone $zoneNameIdx]]} {
        lappend refIds $refId
        break
      }
    }
  }

  return $refIds
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::zones::setRefsByZoneRefId { zoneRefIds } {
  variable referenceCountIdx
  variable zones

  foreach refId $zoneRefIds {
    if {![info exists zones($refId)]} {
      ::tcllogger::logError "::iswmesh::zones::setRefsByZoneRefId" \
        [format "Zone with reference id %d does not exist!" $refId]
    }

    # Get zone from array.
    set zone $zones($refId)

    # Increase reference count.
    set zone [lreplace $zone $referenceCountIdx $referenceCountIdx \
      [expr {[lindex $zone $referenceCountIdx] + 1}] \
    ]

    # Save updated zone in array.
    set zones($refId) $zone
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::zones::setRefsByZoneId { zoneIds } {
  variable zoneIdRefIdMap
  set refIds {}

  foreach zoneId $zoneIds {
    if {![info exists zoneIdRefIdMap($zoneId)]} {
      ::tcllogger::logError "::iswmesh::zones::setRefsByZoneId" \
        [format "Zone with id %d does not exist!" $zoneId]
    }

    set refId [list $zoneIdRefIdMap($zoneId)]
    setRefsByZoneRefId $refId
    lappend refIds $refId
  }

  return $refIds
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::zones::deleteRefsByZoneRefId { zoneRefIds } {
  variable freeRefIds
  variable referenceCountIdx
  variable zoneIdIdx
  variable zones
  variable zoneIdRefIdMap

  foreach refId $zoneRefIds {
    if {![info exists zones($refId)]} {
      ::tcllogger::logError "::iswmesh::zones::deleteRefsByZoneRefId" \
        [format "Zone with reference id %d does not exist!" $refId]
    }

    # Get zone from array.
    set zone $zones($refId)

    # Check if zone should be deleted (if there will be no reference to the
    # zone after this call).
    if {[lindex $zone $referenceCountIdx] <= 1} {
      # Delete the zone, because no one references it.
      set zoneId [lindex $zone $zoneIdIdx]
      array unset zones $refId
      array unset zoneIdRefIdMap $zoneId
      lappend freeRefIds $refId
    } else {
      # Decrease reference count.
      set zone [lreplace $zone $referenceCountIdx $referenceCountIdx \
        [expr {[lindex $zone $referenceCountIdx] - 1}]
      ]

      # Save updated zone in array.
      set zones($refId) $zone
    }
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::zones::deleteRefsByZoneId { zoneIds } {
  variable zoneIdRefIdMap

  foreach zoneId $zoneIds {
    if {![info exists zoneIdRefIdMap($zoneId)]} {
      ::tcllogger::logError "::iswmesh::zones::deleteRefsByZoneId" \
        [format "Zone with id %d does not exist!" $zoneId]
    }

    deleteRefsByZoneRefId [list $zoneIdRefIdMap($zoneId)]
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::zones::convertZonesToPreparsed { dataArray } {
  variable zoneIdIdx
  variable zoneNameIdx
  variable zoneTypeIdx
  variable zones
  upvar $dataArray data

  array set zoneIdSectionMap {}
  set numSections 0
  set currSection -1
  set lastZoneId -1
  set zoneSecBaseName ""

  foreach {refId zone} [array get zones] {
    set zoneId [lindex $zone $zoneIdIdx]

    if {![info exists zoneIdSectionMap($zoneId)]} {
      set zoneIdSectionMap($zoneId) $numSections
      set currSection $numSections
      set zoneSecBaseName "zoneSection-$currSection"
      incr numSections
    } elseif {$lastZoneId != $zoneId} {
      set currSection $zoneIdSectionMap($zoneId)
      set zoneSecBaseName "zoneSection-$currSection"
    }

    set data(${zoneSecBaseName}-zoneId) $zoneId
    set data(${zoneSecBaseName}-zoneType) [lindex $zone $zoneTypeIdx]
    set data(${zoneSecBaseName}-zoneName) [lindex $zone $zoneNameIdx]

    set lastZoneId $zoneId
  }

  set data(numZoneSections) $numSections
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::zones::printZones { {fid -1} } {
  variable zones

  if {$fid == -1} {
    parray zones
  } else {
    ::utils::printArrayToFile $fid zones
  }
}


# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
