# package: icemops
# description:
#   This script file implements a custom mesh data model and operations.

# Initialize package.
source [file join [file dirname [info script]] "init-package.tcl"]

# Dependencies
package require Tcl 8.4
package require utils
package require tcllogger

# Define the namespace.
namespace eval ::iswmesh {
  # Export commands.
  namespace export \
    resetMesh \
    importFluentMesh \
    writeFluentMesh \
    mergeInterfaces \
    renumberMesh \
    printMesh \
    getIdentifier

  # Define namespace variables.
  variable header ""
  variable dimension 0
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::resetMesh {} {
  faces::initialize
  cells::initialize
  nodes::initialize
  zones::initialize
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::importFluentMesh { meshFilePath } {
  variable header ""
  variable dimension 0
  array set data {}

  ::tcllogger::log debug "Parse fluent mesh file $meshFilePath"
  ::fluentmesh::parse $meshFilePath data

  set header $data(header)
  set dimension $data(dimension)

  if {$dimension < 2 || $dimension > 3} {
    ::tcllogger::logError "::iswmesh::importFluentMesh" \
      [format "Invalid dimension \"%d\"!" $dimension]
  }

  # ----------------------------------------------------------------------------
  # Build the zones list, therefore iterate through the zone sections. The
  # entries of the zones list should have the following format:
  #   -> { zoneId zoneName zoneType }
  ::tcllogger::log debug "Build zones list."
  set zonesList {}
  for {set i 0} {$i < $data(numZoneSections)} {incr i} {
    set zoneSecBaseName "zoneSection-$i"

    lappend zonesList [list \
      $data(${zoneSecBaseName}-zoneId) \
      $data(${zoneSecBaseName}-zoneName) \
      $data(${zoneSecBaseName}-zoneType) \
    ]
  }

  # Initialize the zones model.
  ::tcllogger::log debug "Initialize zones model."
  zones::initialize $zonesList

  # ----------------------------------------------------------------------------
  # Build the nodes list, therefore iterate through the node sections. The
  # entries of the nodes list should have the following format:
  #   -> { nodeNumber coordinatesList zoneId nodeType }
  ::tcllogger::log debug "Build nodes list."
  set nodesList {}
  for {set i 0} {$i < $data(numNodeSections)} {incr i} {
    set nodeSecBaseName "nodesSection-$i"

    # Check dimension.
    set ndim $data(${nodeSecBaseName}-nodesDimension)
    if {$ndim > 0 && $ndim != $dimension} {
      ::tcllogger::logError "::iswmesh::importFluentMesh" \
        [format "Invalid nodes dimension \"%d\" of node section %d!" \
        $dimension $i]
    }

    # Iterate through nodes of section.
    foreach node $data(${nodeSecBaseName}-nodes) {
      lappend nodesList [list \
        [lindex $node 0] \
        [lrange $node 1 end] \
        $data(${nodeSecBaseName}-zoneId) \
        $data(${nodeSecBaseName}-nodesType) \
      ]
    }
  }

  # Initialize the nodes model.
  ::tcllogger::log debug "Initialize nodes model."
  nodes::initialize $nodesList


  # ----------------------------------------------------------------------------
  # Build the cells list, therefore iterate through the cell sections. The
  # entries of the cells list should have the following format:
  #   -> { cellNumber zoneId cellType elementType }
  ::tcllogger::log debug "Build cells list."
  set cellsList {}
  for {set i 0} {$i < $data(numCellSections)} {incr i} {
    set cellSecBaseName "cellsSection-$i"

    # Iterate through cells of section.
    foreach cell $data(${cellSecBaseName}-cells) {
      lappend cellsList [list \
        $cell \
        $data(${cellSecBaseName}-zoneId) \
        $data(${cellSecBaseName}-cellsType) \
        $data(${cellSecBaseName}-elementType) \
      ]
    }
  }

  # Initialize the nodes model.
  ::tcllogger::log debug "Initialize cells model."
  cells::initialize $cellsList

  # ----------------------------------------------------------------------------
  # Build the faces list, therefore iterate through the face sections. The
  # entries of the faces list should have the following format:
  #   -> { faceNumber zoneId boundaryCondType faceType n0 ... nN c0 c1 }
  ::tcllogger::log debug "Build faces list."
  set facesList {}
  for {set i 0} {$i < $data(numFaceSections)} {incr i} {
    set faceSecBaseName "facesSection-$i"

    # Iterate through faces of section.
    foreach face $data(${faceSecBaseName}-faces) {
      lappend facesList [concat \
        [lindex $face 0] \
        $data(${faceSecBaseName}-zoneId) \
        $data(${faceSecBaseName}-boundaryCondType) \
        $data(${faceSecBaseName}-faceType) \
        [lrange $face 1 end] \
      ]
    }
  }

  # Initialize the nodes model.
  ::tcllogger::log debug "Initialize faces model."
  faces::initialize $facesList

  # Delete data array
  array unset data
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::writeFluentMesh { filePath } {
  variable header ""
  variable dimension
  array set data {}

  ::tcllogger::log info "Write fluent mesh file $filePath"

  # Renumber mesh.
  ::tcllogger::log info "Renumber mesh ..."
  renumberMesh

  # Convert data model to preparsed data format.
  ::tcllogger::log info "Convert data model to preparsed data format."
  # Convert in header information and dimension.
  set data(header) $header
  set data(dimension) $dimension

  # Convert mesh elements.
  zones::convertZonesToPreparsed data
  nodes::convertNodesToPreparsed data
  cells::convertCellsToPreparsed data
  faces::convertFacesToPreparsed data

  # Write the fluent mesh file.
  ::tcllogger::log info "Write preparsed data to the file."
  ::fluentmesh::write $filePath data

  # Delete data array
  array unset data
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
# {
#   { <tolerance> <interface zone 1 name> <interface zone 2 name> }
#   { <tolerance> <interface zone 3 name> <interface zone 4 name> }
#   ...
# }
# Returns:
proc ::iswmesh::mergeInterfaces { tolInterfacesLists } {
  foreach tolInterfacesList $tolInterfacesLists {
    set tol [lindex $tolInterfacesList 0]

    # Get reference ids of interface zones.
    set intZonesRefIds [zones::getRefIdsByName \
      [lrange $tolInterfacesList 1 2]]

    # Get all nodes of the interface zones.
    ::tcllogger::log debug [format "Get nodes of interface zones %s and %s." \
      [lindex $tolInterfacesList 1] [lindex $tolInterfacesList 2]]

    set nodesOfZones [faces::getNodeRefsOfFacesInZones $intZonesRefIds]
    set intZone1Nodes [lindex $nodesOfZones 0]
    set intZone2Nodes [lindex $nodesOfZones 1]

    # Calculate node merge pairs.
    ::tcllogger::log debug [format "Calculate the node merge pairs (%s%d)." \
      "total number of nodes = " \
      [expr {[llength $intZone1Nodes] + [llength $intZone2Nodes]} ]]
    set nodeMergePairs [calcNodeMergePairs $tol $intZone1Nodes $intZone2Nodes]

    # Merge the nodes.
    ::tcllogger::log debug [format "Merge %d nodes." \
      [expr {[llength $nodeMergePairs]*2} ]]
    nodes::mergeNodes $nodeMergePairs
    #set affectedFaces [nodes::mergeNodes $nodeMergePairs]

    # Update the face references.
    #set facesWithMergedNodes [lindex $affectedFaces 0]
    #set facesMergedTo [lindex $affectedFaces 1]

    ::tcllogger::log debug [format "%d %s" \
      [expr {[llength $nodeMergePairs]*2}] "nodes merged" \
    ]
#    ::tcllogger::log debug [format "%d %s\n%d %s\n%d %s" \
#      [expr {[llength $nodeMergePairs]*2}] "nodes merged" \
#      [llength $facesWithMergedNodes] "faces with merged nodes" \
#      [llength $facesMergedTo] "faces with merged to nodes" \
#    ]

    #::tcllogger::log trace "facesWithMergedNodes: $facesWithMergedNodes"
    #::tcllogger::log trace "facesMergedTo: $facesMergedTo"

    #::tcllogger::log debug "Update references of affected faces."
    #faces::updateFaceReferences $facesWithMergedNodes $facesMergedTo

    # Finished merging of interface zone pair.
    ::tcllogger::log debug [format "Interface zones %s and %s merged" \
      [lindex $tolInterfacesList 1] [lindex $tolInterfacesList 2]]
  }
}

proc mtest {} {
  puts [::utils::createLine "*" 80]
  puts [::utils::createLine "*" 80]

  set mp "/home/georg/saugrohr-fluent.msh"
  set p "/home/georg/saugrohr-intermediate"
  set mpTo "/home/georg/saugrohr-iswfluent.msh"
  set interfaces [list [list 0.01 \
    "SAUGROHR_OBEN_BOTTOM_INTERFACE_3_1" \
    "SAUGROHR_TOP_INTERFACE_1_1" \
  ]]
  ::tcllogger::configure logLevel debug

  puts [::utils::createLine "#" 80]
  puts [::utils::createLine "#" 80]
  ::iswmesh::importFluentMesh $mp

  puts [::utils::createLine "#" 80]
  puts [::utils::createLine "#" 80]
  ::iswmesh::mergeInterfaces $interfaces

  puts [::utils::createLine "#" 80]
  puts [::utils::createLine "#" 80]
  ::iswmesh::printMesh $p

  puts [::utils::createLine "#" 80]
  puts [::utils::createLine "#" 80]
  ::iswmesh::writeFluentMesh $mpTo

  puts [::utils::createLine "*" 80]
  puts [::utils::createLine "*" 80]
}

proc rfs { fs } {
  foreach f $fs {
    if {[info exists faces::faces($f)]} {
      puts "$faces::faces($f)"
    } else {
      puts "face $f does not exist"
    }
  }
}

proc rns { ns } {
  foreach n $ns {
    if {[info exists nodes::nodes($n)]} {
      puts "$nodes::nodes($n)"
    } else {
      puts "node $n does not exist"
    }
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::calcNodeMergePairs { tolerance intZone1Nodes intZone2Nodes } {
  set mergePairs {}

  foreach node1RefId $intZone1Nodes {
    foreach node2RefId $intZone2Nodes {
      # Calculate the distance between node1 and node2.
      set distance [::utils::getVectorsDistance \
        [lindex [nodes::getNodeById $node1RefId] \
          $nodes::coordinatesIdx] \
        [lindex [nodes::getNodeById $node2RefId] \
          $nodes::coordinatesIdx] \
      ]

      ::tcllogger::log trace [format "distance for node pair (%d|%d): %f" \
        $node1RefId $node2RefId $distance]

      # Check if distance is smaller than or equal to the tolerance.
      if {$distance <= $tolerance} {
        # Distance between the nodes is smaller than or equal to the tolerance,
        # therefore add nodes reference ids to the merge pairs list.
        lappend mergePairs [list $node1RefId $node2RefId]
      }
    }
  }

  ::tcllogger::log trace "list of node pairs to merge (node ref ids):"
  ::tcllogger::log trace "$mergePairs"

  return $mergePairs
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::renumberMesh {} {
  # Renumber zones.
  renumberElements \
    zones::zones \
    zones::maxZoneId \
    $zones::zoneIdIdx \
    $zones::maxRefId

  # Renumber nodes.
  renumberElements \
    nodes::nodes \
    nodes::maxNodeNumber \
    $nodes::nodeNumberIdx \
    $nodes::maxId

  # Renumber cells.
  renumberElements \
    cells::cells \
    cells::maxCellNumber \
    $cells::cellNumberIdx \
    $cells::maxId

  # Renumber faces.
  renumberElements \
    faces::faces \
    faces::maxFaceNumber \
    $faces::faceNumberIdx \
    $faces::maxId

  return 1
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::renumberElements {
  elemsArrayName
  maxNumVarName
  numberIdx
  maxId
} {
  upvar $elemsArrayName elemsArray
  upvar $maxNumVarName maxNum
  set num 1

  for {set i 0} {$i <= $maxId} {incr i} {
    if {[info exists elemsArray($i)]} {
      set elemsArray($i) [lreplace $elemsArray($i) $numberIdx $numberIdx $num]
      incr num
    }
  }

  set maxNum [expr {$num - 1}]
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::printMesh { {filePath ""} } {
  variable header
  variable dimension

  if {$filePath == ""} {
    puts [::utils::createLine "-" 80]
    puts [::utils::createLine "-" 80]
    puts "iswmesh, dimension = $dimension, header: $header"

    puts [::utils::createLine "-" 80]
    puts "zones:"
    puts "zones(<refId>) = <refcnt> <zoneId> <zoneName> <zoneType>"
    zones::printZones

    puts [::utils::createLine "-" 80]
    puts "nodes:"
    puts "nodes(<id>) = <refs> <num> <coords> <zoneRefId> <nodeType> <status>"
    nodes::printNodes

    puts [::utils::createLine "-" 80]
    puts "cells"
    puts "cells(<id>) = <refCnt> <num> <zoneRefId> <cellType> <elementType>"
    cells::printCells

    puts [::utils::createLine "-" 80]
    puts "faces"
    puts [format "faces(<id>) = <num> <zoneRefId> <boundaryCondType> %s" \
      "<faceType> <statusInfoList> <cellRefIds> <numNodes> <nodeRefIds>"]
    faces::printFaces

    puts [::utils::createLine "-" 80]
  } else {
    set hasFailed [catch {open $filePath w} fid]
    if {$hasFailed} {
      error [format "\n%s\n%s%s%s\n%s\n" \
        "The procedure \"::utils::printArrayToFile\" failed!" \
        "Failed to open the file \"" $filePath "\": " $fid \
      ]
    }

    puts $fid [::utils::createLine "-" 80]
    puts $fid [::utils::createLine "-" 80]
    puts $fid "iswmesh, dimension = $dimension, header: $header"

    puts $fid [::utils::createLine "-" 80]
    puts $fid "zones:"
    puts $fid "zones(<refId>) = <refcnt> <zoneId> <zoneName> <zoneType>"
    zones::printZones $fid

    puts $fid [::utils::createLine "-" 80]
    puts $fid "nodes:"
    puts $fid "nodes(<id>) = <refs> <num> <coords> <zoneRefId> <nodeType> <status>"
    nodes::printNodes $fid

    puts $fid [::utils::createLine "-" 80]
    puts $fid "cells"
    puts $fid "cells(<id>) = <refCnt> <num> <zoneRefId> <cellType> <elementType>"
    cells::printCells $fid

    puts $fid [::utils::createLine "-" 80]
    puts $fid "faces"
    puts $fid [format "faces(<id>) = <num> <zoneRefId> <boundaryCondType> %s" \
      "<faceType> <cellRefIds> <numNodes> <nodeRefIds>"]
    faces::printFaces $fid

    puts $fid [::utils::createLine "-" 80]
    close $fid
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::iswmesh::getIdentifier { freeIdsList maximumId } {
  upvar $freeIdsList freeIds
  upvar $maximumId maxId
  set newId 0

  set numFreeIds [llength $freeIds]
  if {$numFreeIds > 0} {
    set newId [lindex $freeIds 0]

    if {$numFreeIds == 1} {
      set freeIds {}
    } else {
      set freeIds [lrange $freeIds 1 end]
    }

    if {$newId > $maxId} { set maxId $newId}
  } else {
    set newId [incr maxId]
  }

  return $newId
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
